import argparse
from contextlib import closing
import json
import requests
import shutil
import sys
import urllib.request as request
import pandas as pd
import gzip

commander = argparse.ArgumentParser()
commander.add_argument(
    "-i", "--input", required=False, help="File containing PGS ID to download"
)
commander.add_argument("-p", "--id", required=False, help="PGS ID")
commander.add_argument("-t", "--trait", required=False, help="Phenotype of interest")
commander.add_argument(
    "-o", "--out", default="out", required=False, help="Phenotype of interest"
)
args = vars(commander.parse_args())

if args["input"] is None and args["id"] is None and args["trait"] is None:
    print("Error: You must provide either an input file, a PGS ID or a trait name.\n")
    sys.exit(-1)

# Load in all the IDs
pgs = args["id"]
if args["input"]:
    with open(args["input"]) as fp:
        if pgs is None:
            pgs = [line.rstrip() for line in fp]
        else:
            tmp = pgs
            pgs = [line.rstrip() for line in fp]
            pgs.append(tmp)


def download_pgs(url, id):
    with closing(request.urlopen(url)) as r:
        with open("{0}.gz".format(id), "wb") as f:
            shutil.copyfileobj(r, f)
    df = pd.read_csv(
        "{0}.gz".format(id),
        compression="gzip",
        header=0,
        sep="\t",
        quotechar='"',
        error_bad_lines=False,
        comment="#",
    )
    df.rename(
        columns={
            "chr_name": "CHR",
            "chr_position": "BP",
            "effect_allele": "A1",
            "reference_allele": "A2",
            "effect_weight": "BETA",
            "allelefrequency_effect": "AF",
            "rsID": "SNP",
        },
        inplace=True,
        errors="ignore",
    )
    df.to_csv("{0}.gz".format(id), index=False, compression="gzip", sep="\t")


def process_pgs(ids):
    meta = pd.DataFrame(
        columns=[
            "Trait",
            "ID",
            "Num.SNP",
            "Method",
            "Parameter",
            "GWAS.Population",
            "Training.Population",
            "Genome.Build",
        ]
    )
    for i in ids:
        print("Processing {0}".format(i))
        response = requests.get("https://www.pgscatalog.org/rest/score/{0}".format(i))
        if response.status_code != 200 or len(response.json()) == 0:
            print(
                'Warning: "{0}" not found from PGS catalog. We will ignore it.'.format(
                    i
                )
            )
        else:
            # Get all require information and download the file
            study = response.json()
            gwas = study.get("samples_variants")
            training = study.get("sampels_training")
            gwasPop = "NA" if gwas is None or len(gwas) == 0 else gwas[0].get("ancestry_broad")
            trainPop = "NA" if training is None or len(training) == 0 else training[0].get("ancestry_broad")
            dat = pd.DataFrame(
                [
                    [
                        study.get("trait_reported"),
                        i,
                        study.get("variants_number"),
                        study.get("method_name"),
                        study.get("method_params"),
                        gwasPop,
                        trainPop,
                        study.get("variants_genomebuild"),
                    ],
                ],
                columns=[
                    "Trait",
                    "ID",
                    "Num.SNP",
                    "Method",
                    "Parameter",
                    "GWAS.Population",
                    "Training.Population",
                    "Genome.Build",
                ],
            )
            download_pgs(study.get("ftp_scoring_file"), i)
            meta = meta.append(dat)
    return meta


meta = pd.DataFrame(
    columns=[
        "Trait",
        "ID",
        "Num.SNP",
        "Method",
        "Parameter",
        "GWAS.Population",
        "Training.Population",
        "Genome.Build",
    ]
)

if args["trait"] is not None:
    response = requests.get(
        "https://www.pgscatalog.org/rest/trait/search?term={0}".format(args["trait"])
    )
    if response.status_code != 200 or len(response.json()) == 0:
        print(
            'Warning: no PGS on "{0}" found on PGS catalog. We will ignore it.'.format(
                args["trait"]
            )
        )
    else:
        dat = response.json().get("results")
        ids = set()
        for s in dat:
            ids.update(s.get("associated_pgs_ids"))
            ids.update(s.get("child_associated_pgs_ids"))
        meta = process_pgs(ids)

if pgs is not None:
    # For each ID, download the required data
    meta.append(process_pgs(pgs))

meta.to_csv("{0}.meta".format(args["out"]), index=False)
print("Completed")
# response = requests.get('https://www.pgscatalog.org/rest/score/PGS000004')

# trait_reported
# variants_number
# method_name
# method_params
# variants_genomebuild
# Only record population of training


# with closing(request.urlopen(test.get("ftp_scoring_file"))) as r:
#    with open('file', 'wb') as f:
#        shutil.copyfileobj(r, f)