# Phenotype definitions
In this document, we detailed how we define each phenotype and the covariate included in our study. 

!!! Note
    
    Due to computational cost, we always use the residualized phenotype as our input for downstream analyses

We always include the following covariates:

- 40 Principal Components (PCs)
- Genotyping Batch
- UK Biobank assessment centre

!!! note
    In the following section, phenotype code might include a `%` or `_` which has the following meaning:

    The percent sign (`%`): 0, 1 or multiple numbers of characters

    The underscore (`_`) : single number of character

## Body Mass Index (BMI)

Body Mass Index is extracted directly from the UK biobank (fieldID: [21001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=21001)) using the baseline measurements (instance = 0). 


### Covariates
We consider the full recombination of the following covariates

- **Age** (fieldID: [21003](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=21003))
- **Sex** (fieldID: [31](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=31))
- **Social economical status** (fieldID: [189](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=189))

When Age is included, we also consider adding $Age^2$. In total, there are 11 recombination of covariates tested. 

## Breast Cancer

We used the definition from [Al-Ajmi et al (2018)](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6062099/):

### Cases

Cases are defined as any female who has:

- self-reported breast cancer (`1002` for fieldID: [20001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20001) )
- An ICD10 diagnosis or death record of `C50_` (fieldID: [41202](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41202),[41204](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41204), [41270](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41270), [40001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40001), [40002](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40002))
- An ICD9 diagnosis of `174%` (fieldID: [41271](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41271), [41205](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41205), [41203](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41203))

### Controls

Controls are defined as any female who has:

- Does not report any cancer (no record for fieldID: [20001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20001) )
- Does not have an ICD10 diagnosis of `C%`, `D0%`, `D37%`, `D38%`, `D39%` and `D4%` (fieldID: [41202](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41202),[41204](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41204), [41270](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41270), [40001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40001), [40002](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40002))
- Does not have an ICD9 diagnosis of `1%`, `20%` and `23%` (fieldID: [41271](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41271), [41205](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41205), [41203](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41203))


### Covariates
We consider both including and excluding age as a covariate

- **Age**: 
    - **Cases**: The earliest age of diagnosis or age of death (fieldID: [40001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40001), [41280](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41280), [41281](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41281))
    - **Controls**: The latest recorded age (fieldID: [21003](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=21003))

In total, there are 2 recombination of covariates tested. 

## Coronary Artery Disease (CAD)

We used the definition from [Khera et al (2018)](https://www.nature.com/articles/s41588-018-0183-z):

### Cases
Cases are defined as anyone who has:

- An ICD10 diagnosis or death record of `I21_`,  `I22_`, `I23`, `I24_` or `I252` (fieldID: [41202](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41202),[41204](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41204), [41270](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41270), [40001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40001), [40002](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40002))
- An IC9 diagnosis of `410_`, `411_` or `412_` (fieldID: [41271](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41271), [41205](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41205), [41203](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41203))
- Vascular/heart problems diagnosed by doctor (Heart attack (`1`) for fieldID: [6150](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=6150))
- self reported heart attack (`1075` for fieldID:  [20002](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20002))
- An operation code of `1070, 1095, 1523` (fieldID: [20004](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20004) )
- An operative procedures of `K40_`, `K41_`, `K42_`, `K43_`, `K44_`, `K45_`, `K46_`, `K49_`, `K501` or `K75_` (fieldID: [41272](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41272), [41200](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41200))

### Controls
Anyone who's not cases

### Covariates
We consider the full recombination of the following covariates

- **Age**: 
    - **Cases**: The earliest age of diagnosis or age of death (fieldID: [40001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40001), [41280](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41280), [41281](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41281))
    - **Controls**: The latest recorded age (fieldID: [21003](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=21003))
- **Sex** (fieldID: [31](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=31))

In total, there are 4 recombination of covariates tested. 


## Chronic kidney disease (CKD)
We use the definition from [Zhao et al (2020)](https://bmcmedicine.biomedcentral.com/articles/10.1186/s12916-020-01594-x)

### Cases

Cases are defined as anyone who has 

- An ICD10 diagnosis or death record of `N18`,  `Z992`, `Z940` or `Z49_` (fieldID: [41202](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41202),[41204](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41204), [41270](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41270), [40001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40001), [40002](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40002))
- An IC9 diagnosis of `5859` (fieldID: [41271](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41271), [41205](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41205), [41203](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41203))
- $eGFR < 60 ml/min$  $per 1.73 m^2$, calculated based on the CKD-EPI formula using serum creatinine (fieldID: [30700](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=30700))

The equation is as follow
$$
141 * min(\frac{creatinine}{\kappa}, 1) ^\alpha * max(\frac{creatinine}{\kappa}, 1) ^ {-1.209} * 0.993 ^ {Age} * 1.08 ^ {\beta}
$$
Where $\alpha=-0.329$ for female and $\alpha=-0.411$ for male; $\beta=1$ for female and $\beta=0$ for male; and $\kappa=0.7$ for female and $\kappa=0.9$ for male

!!! note 
    We are supposed to multiply the eGRF by $1.159$ if the sample is an African but we did not, simply because we do not have enough time to drill into such detail. 

### Controls

Controls are defined as anyone who do not

- belong to cases
- have an ICD10 code of `N17%` and `N19%` (fieldID: [41202](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41202),[41204](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41204), [41270](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41270), [40001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40001), [40002](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40002))
- have an ICD9 code of `58%`  (fieldID: [41271](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41271), [41205](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41205), [41203](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41203))

### Covariates

We consider the full recombination of the following covariates

- **Age** (fieldID: [21003](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=21003))
- **Sex** (fieldID: [31](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=31))
- **AUDIT_C** - sum of baseline (instance = 0) answers to fields [20414](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20414), [20403](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20403), [20416](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20416)
- **AUDIT_P** - sum of baseline (instance = 0) answers to fields [20413](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20413), [20407](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20407), [20412](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20412), [20409](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20409), [20408](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20408), [20411](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20411), [20405](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20405)
- **AUDIT_Total** - sum of AUDIT_P and AUDIT_C
We never mix AUDIT_Total, AUDIT_P and AUDIT_C together as a covariate (they are mutually exclusive). In total, there are 15 recombination of covariates tested. 

## Diastolic blood pressure (DBP)
We used the definition from [Evangelou et al (2018)](https://www-nature-com/articles/s41588-018-0205-x):

### Exclusion

- We remove any samples who reported `-1` (do not know) or `-3` (prefer not to answer) on blood pressure medication use (fieldID: [6153](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=6153), [6177](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=6177))

### Definition

1. If there are two automatic measurement, we use the mean of the two measurement (fieldID: [4079](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=4079))
2. Otherwise, if there are two manual measurement, we use the mean of the two measurement (fieldID: [94](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=94))
3. If we only have one automatic and one manual measurement, we use the mean of these two measurement
4. If we only got one measurement, we will used that instead
5. 15 mm Hg is added to anyone who is taking blood pressure medication (a record of `3` for fieldID: [6153](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=6153), [6177](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=6177))


### Covariates
We consider the full recombination of the following covariates

- **Age** (fieldID: [21003](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=21003))
- **Sex** (fieldID: [31](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=31))
- **BMI** (fieldID: [21001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=21001))

When Age is included, we also consider adding $Age^2$. In total, there are 11 recombination of covariates tested. 


## Low-density lipoprotein (LDL)

### Exclusion
We exclude anyone who is taken statin
 
??? note "Statin medication code"
    
    - 1141146234
    - 1141192414
    - 1140910632
    - 1140888594
    - 1140864592
    - 1141146138
    - 1140861970
    - 1140888648
    - 1141192410
    - 1141188146
    - 1140861958
    - 1140881748
    - 1141200040
    - 1140861922

### Definition

LDL phenotype is directly extracted from UK biobank ([30780](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=30780)) using the baseline measurements (instance = 0). 

### Covariates
We consider the full recombination of the following covariates

- **Age** (fieldID: [21003](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=21003))
- **Sex** (fieldID: [31](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=31))
- **BMI** (fieldID: [21001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=21001))
- **Fasting status** (fieldID: [74](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=74))
- **Dilution factor** (fieldID: [30897](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=30897))

In total, there are 32 recombination of covariates tested. 

## Major depressive disorder (MDD)

We used the definition from [Coleman et al (2020)](https://pubmed.ncbi.nlm.nih.gov/31969693/):

### Exclusion
First, we exclude samples who report any of the following mental health problems diagnosed by a professional (fieldID: [20544](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20544)):

- Schizophrenia (`2`)
- Any other type of psychosis or psychotic illness (`3`)
- Mania, hypomania, bipolar or manic-depression (`10`)

### Cases

Cases are defined as anyone who reported that they are :

- affected for most of the day or more during worst episode of depression (`> 2` for fieldID: [20436](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20436))
- depressed (almost) every day during worst episode of depression (`> 1` for fieldID: [20439](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20439))
- more than a little impact on normal roles during worst period of depression (`> 1` for fieldID: [20440](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20440))

and report `5` of the below (at least one of the bold items):

- **Ever had prolonged feelings of sadness or depression** (`Yes` for fieldID: [20446](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20446))
- **Ever had prolonged loss of interest in normal activities** (`Yes` for fieldID: [20441](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20441))
- Feelings of tiredness during worst episode of depression (`Yes` for fieldID: [20449](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20449))
- Weight change during worst episode of depression (`> 0` for fieldID: [20536](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20536))
- Sleep change during worst episode of depression (`Yes` for fieldID: [20532](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20532))
- Difficulty concentrating during worst episode of depression (`Yes` for fieldID: [20435](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20435))
- Feelings of worthlessness during worst episode of depression (`Yes` for fieldID: [20450](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20450))
- Thoughts of death during worst episode of depression (`Yes` for fieldID: [20437](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20437))

### Controls

Controls are defined as anyone who do not:

- Meet case status
- Report any mental health problems diagnosed by a professional (fieldID: [20544](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20544))
- Report depression in previous interview with psychiatric nurse (`1286`, `1291`, `1531` or `1289` to fieldID: [20002](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20002))
- Meet previous criteria for depression or bipolar disorder (`!0` to fieldID: [20126](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20126))
- Have a hospital inpatient ICD10 code for mood disorder (`F3__` for fieldID: [41202](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41202),[41204](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41204))
- Report use of anti-depressant medication at baseline (fieldID: [20003](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20003))
- Report extensive recent symptoms of depression: less than 5 on summed response (where `"not at all" = 1` and `"nearly every day" = 4`) to recent:
    - Lack of interest or pleasure in doing things (fieldID: [20514](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20514))
    - Feelings of inadequacy (fieldID: [20507](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20507))
    - Feelings of depression (fieldID: [20510](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20510))
    - Trouble concentrating on things (fieldID: [20508](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20508))
    - Trouble falling or staying asleep, or sleeping too much (fieldID: [20517](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20517))
    - Changes in speed or amount of moving or speaking (fieldID: [20518](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20518))
    - Feelings of tiredness or low energy (fieldID: [20519](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20519))
    - Thoughts of suicide or self-harm (fieldID: [20513](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20513))
    - Poor appetite or overeating (fieldID: [20511](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20511))

??? note "Depression medication code"

    - 1140879616
    - 1140921600
    - 1140879540
    - 1140867878
    - 1140916282
    - 1140909806
    - 1140867888
    - 1141152732
    - 1141180212
    - 1140879634
    - 1140867876
    - 1140882236
    - 1141190158
    - 1141200564
    - 1140867726
    - 1140879620
    - 1140867818
    - 1140879630
    - 1140879628
    - 1141151946
    - 1140867948
    - 1140867624
    - 1140867756
    - 1140867884
    - 1141151978
    - 1141152736
    - 1141201834
    - 1140867690
    - 1140867640
    - 1140867920
    - 1140867850
    - 1140879544
    - 1141200570
    - 1140867934
    - 1140867758
    - 1140867914
    - 1140867820
    - 1141151982
    - 1140882244
    - 1140879556
    - 1140867852
    - 1140867860
    - 1140917460
    - 1140867938
    - 1140867856
    - 1140867922
    - 1140910820
    - 1140882312
    - 1140867944
    - 1140867784
    - 1140867812
    - 1140867668
    - 1140867940


### Covariates
We consider the full recombination of the following covariates

- **Sex** (fieldID: [31](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=31))
- **BMI** (fieldID: [21001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=21001))
- **Social economical status** (fieldID: [189](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=189))
- **Education**: University (`1`) vs Not university, excluding any sample who refused to provide qualification information (`-3`) (fieldID: [6138](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=6138))

In total, there are 16 recombination of covariates tested. 
## Prostate Cancer

We use similar definition as the [breast cancer](#breast-cancer)

### Cases

Cases are defined as any male who has:

- self-reported prostate cancer (`1044` for fieldID: [20001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20001) )
- An ICD10 diagnosis or death record of `C61%` (fieldID: [41202](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41202),[41204](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41204), [41270](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41270), [40001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40001), [40002](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40002))
- An ICD9 diagnosis of `1859` (fieldID: [41271](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41271), [41205](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41205), [41203](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41203))

### Controls

Controls are defined as any male who has:

- Does not report any cancer (no record for fieldID: [20001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=20001) )
- Does not have an ICD10 diagnosis of `C%`, `D0%`, `D37%`, `D38%`, `D39%` and `D4%` (fieldID: [41202](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41202),[41204](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41204), [41270](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41270), [40001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40001), [40002](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40002))
- Does not have an ICD9 diagnosis of `1%`, `20%` and `23%` (fieldID: [41271](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41271), [41205](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41205), [41203](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41203))


### Covariates
We consider both including and excluding age as a covariate

- **Age**: 
    - **Cases**: The earliest age of diagnosis or age of death (fieldID: [40001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40001), [41280](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41280), [41281](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41281))
    - **Controls**: The latest recorded age (fieldID: [21003](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=21003))

In total, there are 2 recombination of covariates tested. 

## Systolic Blood Pressure (SBP)
We used the definition from [Evangelou et al (2018)](https://www-nature-com/articles/s41588-018-0205-x):

### Exclusion

- We remove any samples who reported `-1` (do not know) or `-3` (prefer not to answer) on blood pressure medication use (fieldID: [6153](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=6153), [6177](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=6177))

### Definition

1. If there are two automatic measurement, we use the mean of the two measurement (fieldID: [4080](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=4080))
2. Otherwise, if there are two manual measurement, we use the mean of the two measurement (fieldID: [93](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=93))
3. If we only have one automatic and one manual measurement, we use the mean of these two measurement
4. If we only got one measurement, we will used that instead
5. 15 mm Hg is added to anyone who is taking blood pressure medication (a record of `3` for fieldID: [6153](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=6153), [6177](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=6177))


### Covariates
We consider the full recombination of the following covariates

- **Age** (fieldID: [21003](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=21003))
- **Sex** (fieldID: [31](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=31))
- **BMI** (fieldID: [21001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=21001))

When Age is included, we also consider adding $Age^2$. In total, there are 11 recombination of covariates tested. 

## Type 2 diabetes (T2D)

### Exclusion

We exclude any individual who has:

- An ICD10 diagnosis or death record of `E10%` or `O24%` (fieldID: [41202](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41202),[41204](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41204), [41270](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41270), [40001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40001), [40002](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40002))
- An IC9 diagnosis of `250_1%`, `250_3` or `648_` (fieldID: [41271](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41271), [41205](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41205), [41203](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41203))

### Cases

Cases are defined as anyone who has:

- An ICD10 diagnosis or death record of `E11%` (fieldID: [41202](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41202),[41204](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41204), [41270](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41270), [40001](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40001), [40002](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=40002))
- An ICD9 diagnosis of `250_`, `250_0` or `250_2` (fieldID: [41271](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41271), [41205](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41205), [41203](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=41203))

### Controls
Controls are defined as anyone who isn't excluded and who are not considered as a case

### Covariates
We consider the full recombination of the following covariates

- **Age** (fieldID: [21003](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=21003))
- **Sex** (fieldID: [31](https://biobank.ctsu.ox.ac.uk/showcase/field.cgi?id=31))

In total, there are 4 recombination of covariates tested. 
