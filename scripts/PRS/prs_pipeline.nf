#!/usr/bin/env nextflow
nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.1'

// Do residualization inverse normalize in all samples together
timestamp='2020-11-19'
if(params.version) {
    System.out.println("")
    System.out.println("Cross-population eMERGE analysis using UK Biobank - Version: $version ($timestamp)")
    exit 1
}
// default values
params.geno = 0.02
params.seed = 1234
params.kmean = 4
params.hwe = 1e-8
params.maf = 0.01
params.build = "grch37"
params.windSize = 200
params.windStep = 50
params.r2 = 0.2
params.maxSize = 10000
params.sex = "sd"
params.sexSD = 3
params.maleF = 0.8
params.femaleF = 0.2
params.thres = 0.044

params.extreme=1
params.normal=5
params.perm=10000
if(params.help){
    System.out.println("")
    System.out.println("Cross-population eMERGE analysis using UK Biobank - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run prs_pipeline.nf [options]")
	System.out.println("Mandatory arguments:")
    System.out.println("    --bgen        BGEN file prefix. Use # to represent chromosome number")
    System.out.println("    --db          UKB Phenotype database")
    System.out.println("    --drop        Samples who withdrawn consent")
    System.out.println("    --greed       GreedyRelated executable")
    System.out.println("    --gwas        GWAS meta file")
    System.out.println("    --script      Script meta file")
    System.out.println("    --cov         Covariate meta file")
    System.out.println("    --ifile       File contain the info score information")
    System.out.println("    --prsice      PRSice executable")
    System.out.println("    --plink       PLINK 2.0 executable")
    System.out.println("    --rel         Path to the relatedness data file")
    System.out.println("    --sample      Sample file for bgen")
    System.out.println("Filtering parameters:")
    System.out.println("    --geno        Genotype missingness. Default: ${params.geno}")
    System.out.println("    --kmean       Number of kmean for pca clustering. Default: ${params.kmean}")
    System.out.println("    --maf         MAF filtering for sample filtering. Default: 0.01")
    System.out.println("    --hwe         HWE filtering. Default: ${params.hwe}")
    System.out.println("    --build       Genome build. Can either be grch37 or grch38. ")
    System.out.println("                  Use to define long LD regions. Default: ${params.build}")
    System.out.println("    --windSize    Window size for prunning. Default: ${params.windSize}")
	System.out.println("    --windStep    Step size for prunning. Default: ${params.windStep}")
    System.out.println("    --r2          Threshold for prunning. Default: ${params.r2}")
    System.out.println("    --maxSize     Maxnumber of samples used for prunning. Default: ${params.maxSize}")
    System.out.println("    --sex         sd or fix.")
    System.out.println("                  sd: exclude samples N sd away from mean, as defined by --sexSD")
    System.out.println("                  fix: exclude male > --maleF and female < --femaleF")
    System.out.println("                  Default: ${params.sex}")
    System.out.println("    --sexSD       Sample with Fstat X SD higher (female)/ lower")
    System.out.println("                  (male) from the mean are filtered. Default: ${params.sexSD}")
    System.out.println("    --maleF       F stat threshold for male. Male with F stat lower")
    System.out.println("                  than this number will be removed. Default: ${params.maleF}")
    System.out.println("    --femaleF     F stat threshold for female. Female with F stat higher")
    System.out.println("                  than this number will be removed. Default: ${params.femaleF}")
    System.out.println("    --relThres    Threshold for removing related samples. Default: ${params.thres}")
    System.out.println("Options:")
    System.out.println("    --seed        Seed for random algorithms. Default: ${params.seed}")
    System.out.println("    --help        Display this help messages")
    exit 1
} 


// include modules

include {   get_country_of_birth;
            get_populations;
            remove_dropout_and_invalid;
            basic_qc;
            generate_high_ld_region;
            prunning;
            calculate_stat_for_sex;
            filter_sex_mismatch;
            finalize_data;
            relatedness_filtering;
            extract_batch;
            outliers_aneuploidy_related;
            extract_pcs;
            generate_covariates;
            extract_biological_sex;
            extract_samples } from './module/quality_control.nf'


include {   pre_clump;
            run_prsice; 
            transform_summary;
            combine_summary;
            bgen_to_bed;
            filter_snps;
            combine_bgen; } from './module/prs_analysis'
            
include {   run_sql;
            residualize } from './module/phenotype'

// Helper function
def fileExists = { fn ->
   if (fn.exists())
       return fn;
    else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}

def gen_file(a, bgen){
    return bgen.replaceAll("#",a.toString())
}
def gen_idx(a, bgen){
    return gen_file(a, bgen)+".bgi"
}

def get_chr(a, bgen){
    if(bgen.contains("#")){
        return a
    }
    else {
        return 0
    }
}
// define variables

bgen = Channel.value(1..22)
    .flatten()
    .map{ a -> [ get_chr(a, "${params.bgen}"), file(gen_file(a, "${params.bgen}")), file(gen_idx(a, "${params.bgen}"))]}
    .unique()
greed = Channel.fromPath("${params.greed}")
mfi = Channel.value(1..22)
    .flatten()
    .map{ a -> [get_chr(a, "${params.ifile}"), file(gen_file(a, "${params.ifile}"))]}
    .unique()
plink = Channel.fromPath("${params.plink}")
prsice = Channel.fromPath("${params.prsice}")
rel = Channel.fromPath("${params.rel}")
sample = Channel.fromPath("${params.sample}")
sql=Channel.fromPath("${params.db}")
withdrawn=Channel.fromPath("${params.dropout}")

// read in the meta files

gwas = Channel.fromPath("${params.gwas}") \
    | splitCsv(header: true)  \
    | map{row -> ["${row.Trait}", file("${row.Path}"), "${row.Pop}", "${row.Remark}"]}  
script = Channel.fromPath("${params.script}") \
    | splitCsv(header: true) \
    | map{row -> ["${row.Trait}", file("${row.SQL}"), "${row.Binary}", file("${row.Rscript}")]} 
covMeta = Channel.fromPath("${params.cov}") \
    | splitCsv(header: true) \
    | map{row -> ["${row.Trait}", "${row.Covariate}"]} 
    
workflow{
    // 1. First, we need to extract the covariate information from UKB
    extract_covariate()
    // 2. Group samples into the 4 population (East Asia will likely not have enough samples)
    extract_population(extract_covariate.out.cov)
    // 3. Reformat BGEN files
    reformat_bgen()
    // 4. Extract the phenotypes
    phenotype_preprocessing(extract_covariate.out.cov)
    // 5. Now do filtering for each trait (we might lost some trait due to sample size)
    quality_control_pipline(
        phenotype_preprocessing.out,
        extract_population.out,
        reformat_bgen.out,
        extract_covariate.out.het, 
        extract_covariate.out.sex)
    // 6. Run PRSice analysis
    prsice_analysis(
        phenotype_preprocessing.out,
        quality_control_pipline.out,
        reformat_bgen.out
    )
}


workflow extract_covariate{
    main:
        // 1. Extract ID of samples with excessive relatedness, excessive heterozygousity and missingness
        //    or sex aneuploidy       
        outliers_aneuploidy_related(sql, "${params.out}")
        // 2. Extract genotyping batch information from the sql
        //    we don't extract centre as assessment centre changed depending on instance
        extract_batch(sql, "${params.out}")
        // 3. Extract all the PCs
        extract_pcs(sql, "${params.out}")
        // 4. Generate the covariate file
        generate_covariates(
            extract_batch.out, 
            extract_pcs.out, 
            "${params.out}")
        // 5. Extract self reported sex
        extract_biological_sex(sql, "${params.out}")
    emit:
        het = outliers_aneuploidy_related.out
        cov = generate_covariates.out
        sex = extract_biological_sex.out
}


workflow extract_population{
    take: covar
    main:
        // 1. First, get country of birth information 
        get_country_of_birth(sql)
        // 2. Extract population based on 4 mean clustering. 
        //    Cluster contains most samples from X are defined
        //    as cluster of X
        get_populations(
            get_country_of_birth.out, 
            covar, 
            params.kmean, 
            params.seed,
            params.out)
        // 3. Group populations together into a single channel so 
        //    that it is easier to handle for downstream analyses
        populations = get_populations.out.eur \
            | mix(get_populations.out.afr) \
            | mix(get_populations.out.asia)
    emit:
        populations
}

workflow reformat_bgen{
    main:
        // 1. Filter out SNPs with info score less than 0.8
        // 2. Then convert the BGEN file into plink files
        mfi \
            | combine(Channel.of(0.8)) \
            | filter_snps \
            | combine(bgen, by: 0) \
            | combine(sample) \
            | combine(plink) \
            | bgen_to_bed \
            | collect \
            | combine_bgen
    emit: 
        combine_bgen.out
}

workflow phenotype_preprocessing{
    take: cov
    main:
        // 1. Run the SQL script
        script \
            | combine(sql) \
            | run_sql
        // 2. Run the R script to get the residual
        run_sql.out \
            | combine(covMeta, by: 0) \
            | combine(cov) \
            | residualize
    emit:
        residualize.out
}

workflow quality_control_pipline{
    // Pheno's format is name of trait, covariate of trait, path to phenotype file 
    take: pheno
    take: pop
    take: genotype
    take: het
    take: sex
    main:
        // 1. Extract samples for each population and each phenotype
        pheno \
            | combine(pop) \
            | combine(withdrawn) \
            | combine(het) \
            | extract_samples
        // 2. Now perform QC
        extract_samples.out \
            | combine(genotype) \
            | combine(Channel.of("${params.hwe}")) \
            | combine(Channel.of("${params.geno}")) \
            | combine(Channel.of("${params.maf}")) \
            | basic_qc
        // 3. Extract samples with relative in data using GreedyRelated and maximize
        //    phenotype retention (already done in filter_sex_mismatch)
        basic_qc.out \
            | combine(greed) \
            | combine(rel) \
            | combine(Channel.of("${params.thres}")) \
            | combine(Channel.of("${params.seed}")) \
            | relatedness_filtering 
        // 4. Obtain the final set of samples
          relatedness_filtering.out \
            | combine(genotype) \
            | finalize_data
    emit:
        finalize_data.out
        
}

workflow prsice_analysis{
    take: pheno
    take: qc
    take: genotype
    main:
        // 1. First, to save time, do pre-clumping using the minimum amount of covarite
        //    which should ensure us to have the largest sample size for each population 
        //    trait combination
        //    This will only run if the trait for that population has more than 100 samples
        qc \
            | combine(gwas, by: 0) \
            | combine(genotype) \
            | combine(prsice) \
            | pre_clump
    
        qc \
            | combine(pheno, by: [0]) \
            | map{ a -> [   a[0],   // phenotype name
                            a[1],   // population
                            a[4],   // Covariate used
                            a[2],   // QCed SNP list
                            a[3],   // QCed sample list
                            a[5]]} \
            | combine(pre_clump.out, by: [0,1] ) \
            | combine(genotype) \
            | combine(prsice) \
            | run_prsice \
            | transform_summary \
            | collect \
            | combine_summary
}
