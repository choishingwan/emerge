#BSUB -L /bin/sh
#BSUB -n 1
#BSUB -J eMERGE
#BSUB -R "span[hosts=1]"
#BSUB -q premium               # target queue for job execution
#BSUB -W 10:00                # wall clock limit for job
#BSUB -P acc_psychgen             # project to charge time
#BSUB -o eMERGE.o
#BSUB -eo eMERGE.e
#BSUB -M 30000
dataArk=/sc/arion/projects/data-ark/ukb/
kcl=${dataArk}/application/ukb18177/
imputed=${kcl}/imputed/
project=/sc/arion/projects/psychgen2/ukb/sam/prs/emerge/
ml java
nextflow run \
    ${project}/scripts/PRS/prs_pipeline.nf \
    --db ${kcl}/phenotype/ukb18177.db \
    --dropout ${kcl}/withdrawn/w18177_20200820.csv \
    --out eMERGE \
    -resume \
    --greed ${dataArk}/software/bin/GreedyRelated \
    --rel ${kcl}/genotyped/ukb18177_rel_s488250.dat \
    --prsice /sc/arion/projects/psychgen/ukb/usr/sam/projects/prs/PRSice/bin/PRSice \
    --bgen ${imputed}/ukb18177_imp_chr#_v3.bgen \
    --ifile ${imputed}/ukb_mfi_chr#_v3.txt \
    --sample ${imputed}/ukb18177_imp_chr1_v3_s487283.sample \
    --plink /sc/arion/projects/psychgen2/ukb/sam/prs/diverse_population/software/plink2 \
    --gwas ${project}/data/sumstat/munged/gwas.meta \
    --cov ${project}/scripts/phenotype/covar.meta \
    --script ${project}/scripts/phenotype/script.meta
