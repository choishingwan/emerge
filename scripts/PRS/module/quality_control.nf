
process extract_samples{
    label 'normal'
    module 'R'
    input:
        tuple   val(name),
                val(cov),
                path(pheno),
                val(pop),
                path(id),
                path(withdrawn),
                path(problem)
    output:
        tuple   val(name),
                val(cov),
                val(pop),
                path("${name}-${pop}.pheno") optional true
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    if(${cov}!="Centre"){

    }else{
        withdrawn <- fread("${withdrawn}", header=F) 
        invalid <- fread("${problem}")
        pop <- fread("${id}")
        pheno <- fread("${pheno}") %>%
            .[!IID %in% withdrawn[,V1]] %>%
            .[!IID %in% invalid[,IID]] %>%
            .[IID %in% pop[,IID]]
        if(nrow(pheno) > 100){
            fwrite(pheno, "${name}-${pop}.pheno", sep="\\t")
        }
    }
    """
}

process extract_batch{
    // We don't extract centre as the centre can change depending on the instance
    label 'normal'
    input:
        path(db)
        val(out)
    output:
        path("${out}.batch")
    script:
    """
    echo "
    .mode csv
    .header on
    .output ${out}.batch
    CREATE TEMP TABLE batch_code
    AS
    SELECT  cm.value AS value,    
            cm.meaning AS meaning 
    FROM    code cm               
    JOIN    data_meta dm ON 
            dm.code_id=cm.code_id
    WHERE   dm.field_id=22000;    
    SELECT      s.sample_id AS FID,
                s.sample_id AS IID,
                COALESCE(
                    batch_code.meaning, 
                    batch.pheno) AS Batch
    FROM        f22000 batch
    JOIN        Participant s
    LEFT JOIN   batch_code ON        
                batch_code.value = batch.pheno
    WHERE       batch.instance=0 AND
                s.sample_id = batch.sample_id AND
                s.withdrawn = 0;
    .quit
        " > sql;
    sqlite3 ${db} < sql
    """
}

process generate_covariates{
    publishDir "phenotype", mode: 'copy', overwrite: true
    module 'R'
    label 'normal'
    input:
        path(batch)
        path(pca)
        val(out)
    output:
        path("${out}.covar")
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    pcs <- fread("${pca}")
    batch <- fread("${batch}")
    pca <- dcast(pcs, FID+IID~Num, value.var="PCs")
    setnames(pca, as.character(c(1:40)), c(paste0("PC",1:40)))
    merge(batch, pca, by = c("FID", "IID")) %>%
        na.omit %>%
        .[, Batch := gsub("\\"", "", Batch)] %>%
        fwrite(., "${out}.covar", sep="\\t")
    """
}


process extract_biological_sex{
    // We don't extract centre as the centre can change depending on the instance
    label 'normal'
    input:
        path(sql)
        val(out)
    output:
        path("${out}.bioSex")
    script:
    """
    echo "
    .mode csv
    .header on
    .output ${out}.bioSex
    
    SELECT      s.sample_id AS FID,
                s.sample_id AS IID,
                sex.pheno AS Sex
    FROM        Participant s
    JOIN        f31 sex ON
                sex.instance = 0 AND
                sex.sample_id = s.sample_ID
    WHERE       s.withdrawn = 0;
    .quit
        " > sql;
    sqlite3 ${sql} < sql
    """
}
process extract_pcs{
    // We don't extract centre as the centre can change depending on the instance
    label 'normal'
    input:
        path(sql)
        val(out)
    output:
        path("${out}.pcs")
    script:
    """
    echo "
    .mode csv
    .header on
    .output ${out}.pcs
    
    SELECT      s.sample_id AS FID,
                s.sample_id AS IID,
                pca.pheno AS PCs,
                pca.array AS Num
    FROM        Participant s
    JOIN        f22009 pca ON
                pca.instance = 0 AND
                pca.sample_id = s.sample_ID
    WHERE       s.withdrawn = 0;
    .quit
        " > sql;
    sqlite3 ${sql} < sql
    """
}
process outliers_aneuploidy_related{
    publishDir "phenotype", mode: 'copy', overwrite: true, pattern: "*outliers"
    label 'normal'
    input:
        path(db)
        val(out)
    output:
        path "${out}.outliers", emit: outliers
    script:
    """
    echo "
    .mode csv
    .header on
    .output ${out}.outliers
    CREATE TEMP TABLE problematic
    AS
    SELECT DISTINCT sample_id 
    FROM(
        SELECT  sample_id
        FROM    f22019 aneuploidy 
        WHERE   aneuploidy.pheno = 1 AND
                aneuploidy.instance = 0
        UNION 
        SELECT  sample_id
        FROM    f22027 outlier 
        WHERE   outlier.pheno = 1 AND
                outlier.instance = 0
        UNION 
        SELECT sample_id
        FROM    f22021 related
        WHERE   related.pheno = 10 AND
                related.instance = 0
    )as subquery;

    SELECT  s.sample_id AS FID,
            s.sample_id AS IID
    FROM    Participant s
    JOIN    problematic ON
            s.sample_id = problematic.sample_id AND
            s.withdrawn = 0;
    .quit
        " > sql;
    sqlite3 ${db} < sql
    """
}


process get_populations{
    publishDir "population", mode: 'copy', overwrite: true
    module 'R'
    input:
        path(pop)
        path(covar)
        val(kmean)
        val(seed)
        val(out)
    output:
        tuple   val("European"),
                path("${out}-${kmean}mean-EUR"), emit: eur
        tuple   val("African"),
                path("${out}-${kmean}mean-AFR"), emit: afr
        tuple   val("Asian"),
                path("${out}-${kmean}mean-ASIA"), emit: asia
        tuple   path("${out}-pca.png"),
                path("${out}-pca-select.png"), emit: plot
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(forcats)
    library(magrittr)
    library(ggplot2)
    library(ggsci)
    # Read in covariate
    cov <- fread("${covar}")
    # Read in population information. Group samples into continent 
    pop <- fread("${pop}") %>%
        .[, Country := floor(as.numeric(Country)/100)] %>%
        .[, Population := as.factor(Country)] %>%
        .[, Population := fct_recode(Population, 
                        "Africa" = "1",
                        "Asia" = "2",
                        "Europe" = "3",
                        "NAmerica" = "4",
                        "Oceania" = "5",
                        "SAmerica" = "6")] %>%
        .[is.na(Population), Population := "UK"] %>%
        .[, -c("Country")]

    # Set the seed for the kmean clustering
    if("${seed}"!="false"){
        set.seed("${seed}")
    }
    # Perform kmean clustering
    pc1k<-kmeans(cov[,PC1], ${kmean})
    pc2k<-kmeans(cov[,PC2], ${kmean})
    cov <- cov[,clusters:=as.factor(paste(pc1k\$cluster,pc2k\$cluster,sep="."))] %>%
        .[,c("FID", "IID", "clusters", "PC1","PC2")]
    population <- merge(cov, pop)
    # Plot PCA plot
    g <- ggplot(cov, aes(x=PC1,y=PC2, color=clusters))+\
            geom_point()+\
            theme_classic()+\
            scale_color_npg()+\
            theme(  legend.text = element_text(size=14),
                    legend.title = element_text(size=16, face="bold"),
                    axis.title = element_text(size=16, face="bold"),
                    axis.text = element_text(size=14))
    ggsave("${out}-pca.png", plot=g, height=7,width=7)

    # We assume the largest cluster is the EUR cluster
    max.cluster <- names(which.max(table(cov[,clusters])))
    eur <- cov[clusters==max.cluster,c("FID", "IID")]
    fwrite(eur, "${out}-${kmean}mean-EUR", quote=F, na="NA", sep="\\t")

    # Then we assume the cluster with most African be the african cluster
    afr.cluster <- names(which.max(table(population[Population=="Africa", clusters])))
    afr <- cov[clusters==afr.cluster,c("FID", "IID")]
    fwrite(afr, "${out}-${kmean}mean-AFR", quote=F, na="NA", sep="\\t")

    # Similarly, we assume the cluster with most Asian are the Asian cluster
    asia.cluster <- names(which.max(table(population[Population=="Asia", clusters])))
    asia <- cov[clusters==asia.cluster,c("FID", "IID")]
    fwrite(asia, "${out}-${kmean}mean-ASIA", quote=F, na="NA", sep="\\t")

    cov[,Group:="Not selected"]
    cov[clusters==max.cluster, Group:="European"]
    cov[clusters==afr.cluster, Group:="African"]
    cov[clusters==asia.cluster, Group:="Asian"]
    g <- ggplot(cov, aes(x=PC1,y=PC2, color=Group))+\
            geom_point()+\
            theme_classic()+\
            scale_color_npg()+\
            theme(  legend.text = element_text(size=14),
                    legend.title = element_text(size=16, face="bold"),
                    axis.title = element_text(size=16, face="bold"),
                    axis.text = element_text(size=14))
    ggsave("${out}-pca-select.png", plot=g, height=7,width=7)
    """
}
process get_country_of_birth{
    publishDir "population", mode: 'copy', overwrite: true
    input:
        path(db)
    output:
        path("population.csv")
    script:
    """
    echo "
    .mode csv
    .header on
    .output population.csv
    SELECT  s.sample_id AS FID,
            s.sample_id AS IID,
            trait.pheno AS Country
    FROM    Participant s
            LEFT JOIN   f20115 trait ON
                        s.sample_id=trait.sample_id
                        AND trait.instance = 0;
    .quit
        " > sql;
    sqlite3 ${db} < sql
    """
}



process remove_dropout_and_invalid{
    module 'R'
    input:
        tuple   path(bed),
                path(bim),
                path(fam)
        path(invalid)
        path(dropout)
        val(out)
    output:
        path "${out}-remove", emit: removed
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    fam <- fread("${fam}") %>%
        setnames(., c("V1", "V2"),c("FID", "IID")) %>%
        .[,c("FID", "IID")]
    invalid <- fread("${invalid}")        
    dropout <- fread("${dropout}", header=F)
    fam <- fam[IID%in% dropout[,V1]]
    rbind(invalid, fam) %>%
        fwrite(., "${out}-remove", sep="\\t")
    """
}

process basic_qc{
    cpus 12
    module 'plink/1.90b6.7'
    executor 'lsf'
    memory '1G'
    input:
        tuple   val(name),
                val(cov),
                val(pop),
                path(pheno),
                path(bed),
                path(bim),
                path(fam),
                val(hwe),
                val(geno),
                val(maf)
    output:
        tuple   val(name),
                val(cov),
                val(pop),
                path("${name}-${pop}-basic-qc.fam"), 
                path("${name}-${pop}-basic-qc.snplist"), emit: qc
    script:
    base=bed.baseName
    """
    plink   --keep ${pheno} \
            --bed ${bed} \
            --bim ${bim} \
            --fam ${fam} \
            --geno ${geno} \
            --maf ${maf} \
            --hwe ${hwe} \
            --write-snplist \
            --make-just-fam \
            --out ${name}-${pop}-basic-qc 
    """
}


process generate_high_ld_region{
    cpus 12
    module 'plink/1.90b6.7'
    memory '1G'
    executor 'lsf'
    input:
        tuple   val(name),
                val(cov),
                val(pop),
                path(sample),
                path(qc_snp),
                path(bed),
                path(bim),
                path(fam),
                val(build)
    output:
        tuple   val(name),
                val(cov),
                val(pop),
                path("${name}-${pop}.set")
    script:
    base=bed.baseName
    """
    echo "1     48000000     52000000   High_LD
2     86000000     100500000    High_LD
2     134500000     138000000   High_LD
2     183000000     190000000   High_LD
3     47500000     50000000 High_LD
3     83500000     87000000 High_LD
3     89000000     97500000 High_LD
5     44500000     50500000 High_LD
5     98000000     100500000    High_LD
5     129000000     132000000   High_LD
5     135500000     138500000   High_LD
6     25000000     35000000 High_LD
6     57000000     64000000 High_LD
6     140000000     142500000   High_LD
7     55000000     66000000 High_LD
8     7000000     13000000  High_LD
8     43000000     50000000 High_LD
8     112000000     115000000   High_LD
10     37000000     43000000    High_LD
11     46000000     57000000    High_LD
11     87500000     90500000    High_LD
12     33000000     40000000    High_LD
12     109500000     112000000  High_LD
20     32000000     34500000 High_LD" > high_ld_37
    echo "1     48060567     52060567     hild
2     85941853     100407914     hild
2     134382738     137882738     hild
2     182882739     189882739     hild
3     47500000     50000000     hild
3     83500000     87000000     hild
3     89000000     97500000     hild
5     44500000     50500000     hild
5     98000000     100500000     hild
5     129000000     132000000     hild
5     135500000     138500000     hild
6     25500000     33500000     hild
6     57000000     64000000     hild
6     140000000     142500000     hild
7     55193285     66193285     hild
8     8000000     12000000     hild
8     43000000     50000000     hild
8     112000000     115000000     hild
10     37000000     43000000     hild
11     46000000     57000000     hild
11     87500000     90500000     hild
12     33000000     40000000     hild
12     109521663     112021663     hild
20     32000000     34500000     hild
X     14150264     16650264     hild
X     25650264     28650264     hild
X     33150264     35650264     hild
X     55133704     60500000     hild
X     65133704     67633704     hild
X     71633704     77580511     hild
X     80080511     86080511     hild
X     100580511     103080511     hild
X     125602146     128102146     hild
X     129102146     131602146     hild" > high_ld_38
    ldFile=high_ld_37
    if [[ "${build}" != "grch37" ]];
    then
        ldFile=high_ld_38
    fi
    echo \${ldFile}
    plink \
        --bed ${bed} \
        --bim ${bim} \
        --fam ${fam} \
        --extract ${qc_snp} \
        --keep ${sample} \
        --make-set \${ldFile} \
        --write-set \
        --out ${name}-${pop}
    """
}


process prunning{
    cpus 12
    memory '1G'
    executor 'lsf'
    module 'plink/1.90b6.7'
    input: 
        tuple   val(name),
                val(cov),
                val(pop),
                path(qc_fam),
                path(qc_snp),
                path(high_ld),
                path(bed),
                path(bim),
                path(fam),
                val(wind_size),
                val(wind_step),
                val(wind_r2),
                val(max_size),
                val(seed)
    output:
        tuple   val(name),
                val(cov),
                val(pop),
                path("${name}-${pop}-qc.prune.in")
    script:
    base=bed.baseName
    """
    if [[ \$(wc -l < ${qc_fam}) -ge ${max_size} ]];
    then
        plink \
            --bed ${bed} \
            --bim ${bim} \
            --fam ${fam} \
            --extract ${qc_snp} \
            --keep ${qc_fam} \
            --indep-pairwise ${wind_size} ${wind_step} ${wind_r2} \
            --out ${name}-${pop}-qc \
            --thin-indiv-count ${max_size} \
            --seed ${seed} \
            --exclude ${high_ld}
    else
        plink \
            --bed ${bed} \
            --bim ${bim} \
            --fam ${fam} \
            --extract ${qc_snp} \
            --keep ${qc_fam} \
            --indep-pairwise ${wind_size} ${wind_step} ${wind_r2} \
            --out ${name}-${pop}-qc \
            --exclude ${high_ld}
    fi 
    """
}  


process calculate_stat_for_sex{
    cpus 12
    memory '1G'
    module 'plink/1.90b6.7'
    executor 'lsf'
    input:
        tuple   val(name),
                val(cov),
                val(pop),
                path(qc_fam),
                path(qc_snp),
                path(prune),
                path(bed),
                path(bim),
                path(fam)
    output:
        tuple   val(name),
                val(cov),
                val(pop),
                path ("${name}-${pop}.sexcheck")
    script:
    base=bed.baseName
    """
    plink \
        --bed ${bed} \
        --bim ${bim} \
        --fam ${fam} \
        --extract ${prune} \
        --keep ${qc_fam} \
        --check-sex \
        --out ${name}-${pop}
    """
}


process filter_sex_mismatch{
    cpus 1
    executor 'lsf'
    memory '1G'
    module 'R'
    input:
        tuple   val(name),
                val(cov),
                val(pop),
                path(qc_fam), 
                path(qc_snp),
                path(fstat),
                path(pheno),
                path (biosex),
                val(mode),
                val(sdm),
                val(male),
                val(female)
    output:
        tuple   val(name), 
                val(cov),
                val(pop),
                path("${name}-${pop}.sex-mismatch"), emit: mismatch
        tuple   val(name),
                val(cov),
                val(pop),
                path("${name}-${pop}.sex-valid"), emit: valid
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    fam <- fread("${qc_fam}")
    pheno <- fread("${pheno}")
    # Read in sex information and remove samples that doesn't pass QC
    sex <- fread("${biosex}") %>%
        merge(., fread("${fstat}")) %>%
        .[FID %in% fam[,V1] & FID > 0] %>%
        .[IID %in% pheno[,IID]]
    filter.info <- NULL
    if("${mode}"=="sd"){
        filter.info <-  "(${sdm} ${mode} from mean)"
        sex.bound <- sex[,.(m=mean(F), s=sd(F)), by="Sex"]
        sex[,invalid := FALSE]
        bound <- sex.bound[Sex=="M"]
        sex[   Sex=="M" &
            ( F < bound[,m] -bound[,s]*${sdm} ), invalid:=TRUE]
        bound <- sex.bound[Sex=="F"]
        sex[   Sex=="F" &
            (F > bound[,m] + bound[,s]*${sdm}), invalid:=TRUE]
    }else{
        filter.info <- "(Male fstat >${male}; Female fstat < ${female} )"
        sex[,invalid:=FALSE]
        sex[Sex=="M" & F < ${male}, invalid:=TRUE ]
        sex[Sex=="F" & F < ${female}, invalid:=TRUE ]
    }
    invalid <- sex[invalid==TRUE]
    fwrite(invalid, "${name}-${pop}.sex-mismatch", sep="\\t")
    fwrite(sex[invalid==FALSE], "${name}-${pop}.sex-valid", sep="\\t")
    """
}


process finalize_data{
    cpus 12
    module 'plink/1.90b6.7'
    memory '1G'
    executor 'lsf'
    input:
        tuple   val(name),
                val(cov),
                val(pop),
                path(qc_snp),
                path(qc_fam),
                path(bed),
                path(bim),
                path(fam)
    output:
        tuple   val(name),
                val(pop),
                path("${name}-${pop}-qc.snplist"),
                path("${name}-${pop}-qc.fam")
    script:
    base=bed.baseName
    """
    plink \
        --bed ${bed} \
        --bim ${bim} \
        --fam ${fam} \
        --extract ${qc_snp} \
        --keep ${qc_fam} \
        --make-just-fam \
        --write-snplist \
        --out ${name}-${pop}-qc
    """
}


process relatedness_filtering{
    cpus 1
    executor 'lsf'
    memory '10G'
    time '1h'
    module 'cmake'
    input: 
        tuple   val(name),
                val(cov),
                val(pop),
                path(samples),
                path(snps),
                path(greedy),
                path(related),
                val(thres),
                val(seed)
    output:
        tuple   val(name),
                val(cov),
                val(pop),
                path(snps),
                path("${name}-${pop}-valid.samples")
    script:
    """

    ./${greedy} \
        -r ${related} \
        -i ID1 \
        -I ID2 \
        -f Kinship \
        -k ${samples} \
        -o ${name}-${pop}-invalid.samples \
        -t ${thres} \
        -s ${seed}
    
    awk 'NR==FNR{a[\$2]=\$2} NR != FNR && \$2 in a{print}' ${name}-${pop}-invalid.samples ${samples} > ${name}-${pop}-valid.samples
    """
}
