
process residualize{
    label 'normal'
    module 'R'
    input:
        tuple   val(name),
                val(binary),
                path(script),
                path(pheno),
                val(cov),
                path(covFile)
    output:
        tuple   val(name),
                val(cov),
                path("${name}.adj.pheno")
    """
    Rscript ${script} ${name} ${binary} ${covFile} ${cov}
    """
            
}

process run_sql{
    label 'normal'
    time '5h'
    input:
        tuple   val(name),
                path(sql),
                val(binary),
                path(rscript),
                path(db)
    output:
        tuple   val(name),
                val(binary),
                path(rscript),
                path("${name}.csv")
    script:
    """
    sqlite3 ${db} < ${sql}
    """
}
