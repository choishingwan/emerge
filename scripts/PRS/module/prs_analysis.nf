// This module contains processes for Polygenic score analysis
process transform_summary{
    module 'R'
    label 'small'
    input:
        tuple   val(name),
                val(covar),
                val(pop),
                val(gwasPop),
                val(gwasRemark),
                path(summary),
                path(best),
                path(pheno)
    output:
        path("${name}-${covar}-${pop}-${gwasPop}-${gwasRemark}")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    library(forcats)
    get_quantile <- function(x, num.quant) {
        quant <- as.numeric(cut(
            x,
            breaks = unique(quantile(x, probs = seq(
                0, 1, 1 / num.quant
            ))),
            include.lowest = T
        ))
        return(quant)
    }
    pheno <- fread("${pheno}")
    prs <- fread("${best}") %>%
        .[In_Regression == "Yes"] %>%
        merge(., pheno) %>%
        .[,Quant:=get_quantile(PRS, 100)] %>%
        .[, Group := "Normal"] %>%
        .[Quant==100, Group := "Extreme"] %>%
        .[, Group := as.factor(Group)] %>%
        .[, Group := fct_relevel(Group, "Normal")]
    reg <- summary(lm(Pheno~Group, prs))
    coef.res <- reg\$coefficients[2, 1]
    ci <- (1.96 * reg\$coefficients[2, 2])
    p <- reg\$coefficients[2, 4]
    ci.u <- exp(coef.res + ci)
    ci.l <- exp(coef.res - ci)
    coef.res <- exp(coef.res)

    fread("${summary}") %>% 
        .[,c("PRS.R2", "P", "Num_SNP")] %>%
        .[, Population := "${pop}" ] %>%
        .[, Covariates := gsub("\\\\+", ",", "${cov}")] %>%
        .[, Base.Population := "${gwasPop}"] %>%
        .[, Base.Remark := "${gwasRemark}"] %>%
        .[, Trait := "${name}"] %>%
        .[, Top.Rest.OR := coef.res] %>%
        .[, Top.Rest.CI := paste0(ci.l, ci.u,sep="-")] %>%
        .[, Top.Rest.P := p] %>%
        fwrite(., "${name}-${covar}-${pop}-${gwasPop}-${gwasRemark}")
    """
}

process combine_summary{
    publishDir "result", mode: 'copy', overwrite: true
    label 'small'
    module 'R'
    input:
        path("*")
    output:
        path("PRSice.result")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    files <- list.files()
    res <- NULL
    for(i in files){
        res %<>% rbind(., fread(i))
    }
    fwrite(res, "LDL.result")
    """
}

process filter_snps{
    label 'normal'
    module 'R'
    input:
        tuple   val(chr),
                path(mfi),
                val(info)
    output:
        tuple   val(chr),
                path("chr${chr}.snp")
    script:
    """
     #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    dat <- fread("${mfi}", header = F) %>%
        .[V8 > ${info}] %>%
        setnames(., "V2", "SNP") %>%
        .[,SNP]
    fwrite(list(dat), "chr${chr}.snp")
    """
}
process combine_bgen{
    label 'huge'
    module 'plink/1.90b6.7'
    input:
        path("*")
    output:
        tuple   path("geno.bed"),
                path("geno.bim"),
                path("geno.fam")
    script:
    """
    ls *bed | sed 's/.bed//g' | awk '{print \$1".bed "\$1".bim "\$1".fam"}' > list 
    plink \
        --merge-list list \
        --make-bed \
        --out geno
    """
}
process bgen_to_bed{
    label 'huge'
    time '10h'
    module 'plink/1.90b6.7'
    input:
        tuple   val(chr),
                path(snp),
                path(bgen),
                path(bgi),
                path(sample),
                path(plink)
    output:
        tuple   path("chr${chr}.bed"),
                path("chr${chr}.bim"),
                path("chr${chr}.fam")
    script:
    """
     ./${plink} \
        --bgen ${bgen} ref-first \
        --sample ${sample} \
        --extract ${snp} \
        --make-bed \
        --out chr${chr}.tmp \
        --threads 16
    cut -f 2 chr${chr}.tmp.bim | awk '{if(\$1 in a){print \$1}else{a[\$1]=\$1}}' > remove
    plink \
        --bfile chr${chr}.tmp \
        --exclude remove \
        --make-bed \
        --out chr${chr}
    rm *tmp*
    """

}
process pre_clump{
    memory { 5.GB + (1.GB * task.attempt) }
    label 'big'
    module 'cmake'
    time '10h'
    afterScript 'ls * | grep -v *snp | grep -v *fam | xargs rm'
    input:
        tuple   val(name),
                val(pop),
                path(qc_snp),
                path(qc_fam), 
                path(gwas),
                val(gwasPop),
                val(gwasRemark),
                path(bed),
                path(bim),
                path(fam),
                path(prsice)
    output:
        tuple   val(name),
                val(pop),
                val(gwasPop),
                val(gwasRemark),
                path(gwas),
                path("${name}-${pop}.snp")  optional true
    script:
    base=bed.baseName

    // First go for base duplication, second go for
    // duplication in target 
    """
    if [[ \$(wc -l < ${qc_fam}) -gt 100 ]];
    then
        ./${prsice} \
            --target ${base} \
            --base ${gwas} \
            --snp SNP \
            --a1 A1 \
            --a2 A2 \
            --beta \
            --stat BETA \
            --fastscore \
            --bar-levels 1 \
            --no-regress \
            --print-snp \
            --keep ${qc_fam} \
            --pvalue P \
            --thread 22 \
            --clump-only \
            --chr-id c:L \
            --out ${name}-${pop} ||
        ./${prsice} \
            --target ${base} \
            --base ${gwas} \
            --snp SNP \
            --a1 A1 \
            --a2 A2 \
            --beta \
            --stat BETA \
            --extract ${name}-${pop}.valid \
            --fastscore \
            --bar-levels 1 \
            --no-regress \
            --print-snp \
            --keep ${qc_fam} \
            --clump-only \
            --pvalue P \
            --thread 22 \
            --chr-id c:L \
            --out ${name}-${pop} ||
        ./${prsice} \
            --target ${base} \
            --base ${gwas} \
            --snp SNP \
            --a1 A1 \
            --a2 A2 \
            --beta \
            --stat BETA \
            --extract ${name}-${pop}.valid \
            --fastscore \
            --bar-levels 1 \
            --no-regress \
            --print-snp \
            --keep ${qc_fam} \
            --clump-only \
            --pvalue P \
            --thread 22 \
            --chr-id c:L \
            --out ${name}-${pop}
    fi
    """
}

process run_prsice{
    label 'more'
    module 'cmake'
    time '12h'
    input:
        tuple   val(name),
                val(pop),
                val(covar),
                path(qc_snp),
                path(qc_fam), 
                path(pheno),
                val(gwasPop),
                val(gwasRemark),
                path(gwas),
                path(clumped),
                path(bed),
                path(bim),
                path(fam),
                path(prsice)
    output:
        tuple   val(name),
                val(covar),
                val(pop),
                val(gwasPop),
                val(gwasRemark),
                path("${name}-${pop}.summary"),
                path("${name}-${pop}.best"),
                path(pheno)
                
    script:
    base = bed.baseName
    """
    ./${prsice} \
        --target ${base} \
        --base ${gwas} \
        --snp SNP \
        --a1 A1 \
        --a2 A2 \
        --beta \
        --stat BETA \
        --pheno ${pheno} \
        --extract ${clumped} \
        --no-clump \
        --keep ${qc_fam} \
        --pvalue P \
        --chr-id c:L \
        --out ${name}-${pop} \
        --ultra
    """
}
