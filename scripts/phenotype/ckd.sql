.mode csv
.output CKD.csv
.header on


CREATE TEMP TABLE Age_Diagnosis AS
SELECT birth.sample_id AS sample_id,
    age.array AS array,
    strftime('%Y', age.pheno) - CAST(birth.pheno AS INT) AS age,
    "ICD10" AS ICD
FROM f34 birth
    JOIN f41280 age ON age.sample_id = birth.sample_id
UNION ALL
SELECT birth.sample_id AS sample_id,
    age.array AS array,
    strftime('%Y', age.pheno) - CAST(birth.pheno AS INT) AS age,
    "ICD9" AS ICD
FROM f34 birth
    JOIN f41281 age ON age.sample_id = birth.sample_id
UNION ALL
SELECT sample_id,
    CAST(pheno AS INT) AS age,
    instance AS array,
    "Self" AS ICD
FROM f20001
UNION ALL
SELECT death.sample_id AS sample_id,
    CAST(death.pheno AS INT) AS age,
    0 AS array,
    "Death" AS ICD
FROM f40001 death
GROUP BY death.sample_id
HAVING COUNT(DISTINCT death.pheno) = 1; -- don't allow multiple age of death

-- We won't be able to actually define CKD phenotype here as we need to calculate the 
-- eGFR, which is not possible in SQLite as we don't have the power function
CREATE TEMP TABLE eGFR AS
SELECT c.sample_id AS sample_id,
    CAST(c.pheno AS INT) * 0.011312217194570135 AS creatinine,
    age.pheno AS Age,
    c.instance AS Instance,
    sex.pheno AS Sex,
    centre.pheno AS Centre
FROM f30700 c
    JOIN f31 sex ON c.sample_id = sex.sample_id
    AND c.instance = sex.instance
    JOIN f21003 age ON c.sample_id = age.sample_id
    AND c.instance = age.instance
    JOIN f54 centre ON c.sample_id = centre.sample_id
    AND c.instance = centre.instance
WHERE c.instance = (SELECT min(instance) FROM f30700 )
GROUP BY c.sample_id;
    
CREATE TEMP TABLE CKD AS
SELECT ICD.sample_id AS sample_id,
    1 AS Pheno,
    MIN(ICD.age) AS Age
FROM (
        SELECT icd10.sample_id AS sample_id,
            MIN(age.age) AS age
        FROM f41204 icd10 -- Diagnoses - secondary ICD10
            JOIN Age_Diagnosis age ON icd10.sample_id = age.sample_id
            AND icd10.array = age.array
            AND age.ICD = "ICD10"
        WHERE icd10.pheno LIKE '"N18"'
            OR icd10.pheno LIKE '"Z992"'
            OR icd10.pheno LIKE '"Z940"'
            OR icd10.pheno LIKE '"Z49_"'
        GROUP BY icd10.sample_id 
        -------------------------------------------------------
        UNION
        SELECT icd10.sample_id AS sample_id,
            MIN(age.age) AS age
        FROM f41202 icd10 -- Diagnoses - main ICD10
            JOIN Age_Diagnosis age ON icd10.sample_id = age.sample_id
            AND icd10.array = age.array
            AND age.ICD = "ICD10"
        WHERE icd10.pheno LIKE '"N18"'
            OR icd10.pheno LIKE '"Z992"'
            OR icd10.pheno LIKE '"Z940"'
            OR icd10.pheno LIKE '"Z49_"'
        GROUP BY icd10.sample_id 
        -------------------------------------------------------
        UNION
        SELECT icd10.sample_id AS sample_id,
            MIN(age.age) AS age
        FROM f41270 icd10 -- Diagnoses - ICD10
            JOIN Age_Diagnosis age ON icd10.sample_id = age.sample_id
            AND icd10.array = age.array
            AND age.ICD = "ICD10"
        WHERE icd10.pheno LIKE '"N18"'
            OR icd10.pheno LIKE '"Z992"'
            OR icd10.pheno LIKE '"Z940"'
            OR icd10.pheno LIKE '"Z49_"'
        GROUP BY icd10.sample_id 
        -------------------------------------------------------
        UNION
        SELECT icd10.sample_id AS sample_id,
            MIN(age.age) AS age
        FROM f40002 icd10 -- Contributory (secondary) causes of death: ICD10
            JOIN Age_Diagnosis age ON icd10.sample_id = age.sample_id
            AND age.ICD = "Death"
        WHERE icd10.pheno LIKE '"N18"'
            OR icd10.pheno LIKE '"Z992"'
            OR icd10.pheno LIKE '"Z940"'
            OR icd10.pheno LIKE '"Z49_"'
        GROUP BY icd10.sample_id 
        -------------------------------------------------------
        UNION
        SELECT icd10.sample_id AS sample_id,
            MIN(age.age) AS age
        FROM f40001 icd10 -- Underlying (primary) cause of death: ICD10
            JOIN Age_Diagnosis age ON icd10.sample_id = age.sample_id
            AND age.ICD = "Death"
        WHERE icd10.pheno LIKE '"N18"'
            OR icd10.pheno LIKE '"Z992"'
            OR icd10.pheno LIKE '"Z940"'
            OR icd10.pheno LIKE '"Z49_"'
        GROUP BY icd10.sample_id 
        -------------------------------------------------------
        UNION
        SELECT icd9.sample_id AS sample_id,
            MIN(age.age) AS age
        FROM f41271 icd9 -- Diagnoses - ICD9
            JOIN Age_Diagnosis age ON icd9.sample_id = age.sample_id
            AND icd9.array = age.array
            AND age.ICD = "ICD9"
        WHERE icd9.pheno LIKE '"5859"'
        GROUP BY icd9.sample_id 
        -------------------------------------------------------
        UNION
        SELECT icd9.sample_id AS sample_id,
            MIN(age.age) AS age
        FROM f41205 icd9 -- Diagnoses - secondary ICD9
            JOIN Age_Diagnosis age ON icd9.sample_id = age.sample_id
            AND icd9.array = age.array
            AND age.ICD = "ICD9"
        WHERE icd9.pheno LIKE '"5859"'
        GROUP BY icd9.sample_id 
        -------------------------------------------------------
        UNION
        SELECT icd9.sample_id AS sample_id,
            MIN(age.age) AS age
        FROM f41203 icd9 -- Diagnoses - main ICD9
            JOIN Age_Diagnosis age ON icd9.sample_id = age.sample_id
            AND icd9.array = age.array
            AND age.ICD = "ICD9"
        WHERE icd9.pheno LIKE '"5859"'
        GROUP BY icd9.sample_id
        -------------------------------------------------------
        UNION
        SELECT self.sample_id AS sample_id,
            MIN(age.age) AS age
        FROM f20002 self -- Non cancer code, self-reported
            JOIN Age_Diagnosis age ON self.sample_id = age.sample_id
            AND self.instance = age.array
            AND age.ICD = "Self"
        WHERE self.pheno = 1192  
            OR self.pheno = 1193
        GROUP BY self.sample_id 
    ) AS ICD
GROUP BY ICD.sample_id;

CREATE TEMP TABLE AuditTotal AS
SELECT  s.sample_id as sample_id,
        (   COALESCE(q1.pheno,0)+
            COALESCE(q2.pheno,0)+
            COALESCE(q3.pheno,0)+
            COALESCE(q4.pheno,0)+
            COALESCE(q5.pheno,0)+
            COALESCE(q6.pheno,0)+
            COALESCE(q7.pheno,0)+
            COALESCE(q8.pheno,0)+
            COALESCE(q9.pheno,0)+
            COALESCE(q10.pheno,0)) as AUDIT_Total,
        (   COALESCE(q1.pheno,0)+
            COALESCE(q2.pheno,0)+
            COALESCE(q3.pheno,0)) as AUDIT_C,
        (   COALESCE(q4.pheno,0)+
            COALESCE(q5.pheno,0)+
            COALESCE(q6.pheno,0)+
            COALESCE(q7.pheno,0)+
            COALESCE(q8.pheno,0)+
            COALESCE(q9.pheno,0)+
            COALESCE(q10.pheno,0)) as AUDIT_P
FROM    Participant s 
        LEFT JOIN f20414 q1 ON
            s.sample_id=q1.sample_id 
            AND q1.Instance=0 
            AND q1.Pheno >=0
        LEFT JOIN f20403 q2 ON
            s.sample_id=q2.sample_id 
            AND q2.Instance=0 
            AND q2.Pheno >=0
        LEFT JOIN f20416 q3 ON
            s.sample_id=q3.sample_id 
            AND q3.Instance=0 
            AND q3.Pheno >=0
        LEFT JOIN f20413 q4 ON
            s.sample_id=q4.sample_id 
            AND q4.Instance=0 
            AND q4.Pheno >=0
        LEFT JOIN f20407 q5 ON
            s.sample_id=q5.sample_id 
            AND q5.Instance=0 
            AND q5.Pheno >=0
        LEFT JOIN f20412 q6 ON
            s.sample_id=q6.sample_id 
            AND q6.Instance=0 
            AND q6.Pheno >=0
        LEFT JOIN f20409 q7 ON
            s.sample_id=q7.sample_id 
            AND q7.Instance=0 
            AND q7.Pheno >=0
        LEFT JOIN f20408 q8 ON
            s.sample_id=q8.sample_id 
            AND q8.Instance=0 
            AND q8.Pheno >=0
        LEFT JOIN f20411 q9 ON
            s.sample_id=q9.sample_id 
            AND q9.Instance=0 
            AND q9.Pheno >=0
        LEFT JOIN f20405 q10 ON
            s.sample_id=q10.sample_id 
            AND q10.Instance=0 
            AND q10.Pheno >=0
WHERE   (
            q1.pheno IS NOT NULL
            OR q2.pheno IS NOT NULL
            OR q3.pheno IS NOT NULL
        )
        AND (
            q4.pheno IS NOT NULL
            OR q5.pheno IS NOT NULL
            OR q6.pheno IS NOT NULL
            OR q7.pheno IS NOT NULL
            OR q8.pheno IS NOT NULL
            OR q9.pheno IS NOT NULL
            OR q10.pheno IS NOT NULL
        );


CREATE TEMP TABLE Controls AS
SELECT s.sample_id AS sample_id,
    0 AS Pheno,
    MAX(age.Pheno) AS Age
FROM Participant s
    JOIN f21003 age ON age.sample_id = s.sample_id
WHERE s.withdrawn = 0
    AND s.sample_id NOT IN (
        SELECT sample_id
        FROM CKD -- CKD Cases
        -------------------------------------------------------
        UNION ALL
        SELECT sample_id
        FROM f41204 -- Diagnoses - secondary ICD10
        WHERE pheno LIKE '"N17%"'
            OR pheno LIKE '"N19%"'
        -------------------------------------------------------
        UNION ALL
        SELECT sample_id
        FROM f41202 -- Diagnoses - main ICD10
        WHERE pheno LIKE '"N17%"'
            OR pheno LIKE '"N19%"'
        -------------------------------------------------------
        UNION ALL
        SELECT sample_id
        FROM f41270 -- Diagnoses - ICD10
        WHERE pheno LIKE '"N17%"'
            OR pheno LIKE '"N19%"'
        -------------------------------------------------------
        UNION ALL
        SELECT sample_id
        FROM f40002 -- Contributory (secondary) causes of death: ICD10
        WHERE pheno LIKE '"N17%"'
            OR pheno LIKE '"N19%"'
        -------------------------------------------------------
        UNION ALL
        SELECT sample_id
        FROM f40001 -- Underlying (primary) cause of death: ICD10
        WHERE pheno LIKE '"N17%"'
            OR pheno LIKE '"N19%"'
        -------------------------------------------------------
        UNION ALL
        SELECT sample_id
        FROM f41271 -- Diagnoses - ICD9
        WHERE pheno LIKE '"58%"'
        -------------------------------------------------------
        UNION ALL
        SELECT sample_id
        FROM f41205 -- Diagnoses - secondary ICD9
        WHERE pheno LIKE '"58%"'
        -------------------------------------------------------
        UNION ALL
        SELECT sample_id
        FROM f41203 -- Diagnoses - main ICD9
        WHERE pheno LIKE '"58%"'
    )
GROUP BY s.sample_id;



INSERT INTO CKD(sample_id, pheno, age)
SELECT *
FROM Controls;

DROP TABLE Controls;

/* Now obtain the required data structure*/
CREATE TEMP TABLE RESULT AS
SELECT s.sample_id as FID,
    s.sample_id as IID,
    ckd.age as CKD_Age,
    g.age as Age,
    g.Sex as Sex,
    g.Centre as Centre,
    g.creatinine AS creatinine,
    ckd.pheno as CKD,
    AuditTotal.AUDIT_Total AS AUDIT_Total,
    AuditTotal.AUDIT_C AS AUDIT_C,
    AuditTotal.AUDIT_P AS AUDIT_P
FROM Participant s
    JOIN CKD ckd ON s.sample_id = ckd.sample_id
    LEFT JOIN eGFR g ON s.sample_id = g.sample_id
    LEFT JOIN AuditTotal ON s.sample_id = AuditTotal.sample_id
WHERE s.withdrawn = 0;

SELECT * FROM RESULT;
.quit