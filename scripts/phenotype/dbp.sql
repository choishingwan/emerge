.mode csv
.output DBP.csv
.header ON

CREATE TEMP TABLE drug AS
SELECT sample_id,
    pheno
FROM (
        SELECT sample_id,
            (
                CASE
                    WHEN pheno = 3 THEN 10
                    ELSE 0
                END
            ) AS pheno
        FROM f6153
        WHERE pheno != -1
            AND pheno != -3
            AND instance = 0
        UNION
        SELECT sample_id,
            (
                CASE
                    WHEN pheno = 3 THEN 10
                    ELSE 0
                END
            ) AS pheno
        FROM f6177
        WHERE pheno != -1
            AND pheno != -3
            AND instance = 0
    )
WHERE sample_id NOT IN(
        SELECT sample_id
        FROM f6153
        WHERE (
                pheno = -1
                OR pheno = -3
            )
            AND instance = 0
        UNION
        SELECT sample_id
        FROM f6177
        WHERE (
                pheno = -1
                OR pheno = -3
            )
            AND instance = 0
    );


SELECT s.sample_id AS FID,
    s.sample_id AS IID,
    (
        CASE
            WHEN COUNT(a.pheno) > COUNT(m.pheno) THEN AVG(a.pheno) + d.pheno
            ELSE (
                CASE
                    WHEN COUNT(m.pheno) > COUNT(a.pheno) THEN AVG(m.pheno) + d.pheno
                    ELSE (
                        CASE
                            WHEN COUNT(m.pheno) = COUNT(m.pheno)
                            AND COUNT(m.pheno) = 1 THEN (m.pheno + a.pheno) / 2 + d.pheno
                        END
                    )
                END
            )
        END
    ) AS DBP,
    age.pheno AS Age,
    sex.pheno AS Sex,
    bmi.pheno AS BMI,
    centre.pheno AS Centre,
    age.pheno * age.pheno AS Age2
FROM Participant s
    LEFT JOIN f4079 a ON a.sample_id = s.sample_id
    AND a.instance = 0
    LEFT JOIN f94 m ON m.sample_id = s.sample_id
    AND m.instance = 0
    JOIN drug d ON d.sample_id = s.sample_id
    JOIN f31 sex ON s.sample_id = sex.sample_id
    AND sex.instance = 0
    JOIN f21003 age ON s.sample_id = age.sample_id
    AND age.instance = 0
    JOIN f54 centre ON s.sample_id = centre.sample_id
    AND centre.instance = 0
    LEFT JOIN f21001 bmi ON s.sample_id = bmi.sample_id
    AND bmi.instance = 0
WHERE s.withdrawn = 0
GROUP BY s.sample_id;