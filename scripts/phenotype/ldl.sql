.mode csv
.header on
.output LDL.csv 

CREATE TEMP TABLE ldl AS
SELECT s.sample_id AS FID,
    s.sample_id AS IID,
    age.pheno AS Age,
    sex.pheno AS Sex,
    centre.pheno AS Centre,
    fasting.pheno AS Fasting,
    dilution.pheno AS Dilution,
    trait.pheno AS LDL,
    bmi.pheno AS BMI,
    MAX(
        CASE
            WHEN med.instance = 0 THEN CASE
                WHEN med.pheno in (
                    1141146234,
                    1141192414,
                    1140910632,
                    1140888594,
                    1140864592,
                    1141146138,
                    1140861970,
                    1140888648,
                    1141192410,
                    1141188146,
                    1140861958,
                    1140881748,
                    1141200040,
                    1140861922
                ) THEN 1
                ELSE 0
            END
        END
    ) AS Statin
FROM Participant s
    JOIN f20003 med ON s.sample_id = med.sample_id
    LEFT JOIN f30780 trait ON s.sample_id = trait.sample_id
    AND trait.instance = 0
    LEFT JOIN f31 sex ON s.sample_id = sex.sample_id
    AND sex.instance = 0
    LEFT JOIN f21003 age ON s.sample_id = age.sample_id
    AND age.instance = 0
    LEFT JOIN f54 centre ON s.sample_id = centre.sample_id
    AND centre.instance = 0
    LEFT JOIN f74 fasting ON s.sample_id = fasting.sample_id
    AND fasting.instance = 0
    LEFT JOIN f30897 dilution ON s.sample_id = dilution.sample_id
    AND dilution.instance = 0
    LEFT JOIN f21001 bmi ON s.sample_id = bmi.sample_id
    AND bmi.instance = 0
WHERE s.withdrawn = 0
GROUP BY s.sample_id;

SELECT *
FROM ldl
WHERE Statin = 0;