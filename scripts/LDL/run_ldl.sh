#BSUB -L /bin/sh
#BSUB -n 1
#BSUB -J eMERGE
#BSUB -R "span[hosts=1]"
#BSUB -q premium               # target queue for job execution
#BSUB -W 10:00                # wall clock limit for job
#BSUB -P acc_psychgen             # project to charge time
#BSUB -o eMERGE.o
#BSUB -eo eMERGE.e
#BSUB -M 30000
imputed=/sc/arion/projects/data-ark/ukb/application/ukb18177/imputed/
kcl=/sc/arion/projects/psychgen/ukb/applications/kcl/
ml java
nextflow run \
    ../scripts/LDL/ldl_analysis.nf \
    --db ${kcl}/ukb18177.db \
    --dropout ${kcl}/dropout/w18177_20200204.csv \
    --sqc ${kcl}/ukb18177_sqc_v2.txt \
    --bfile ${kcl}/ukb18177 \
    --out eMERGE \
    -resume \
    --greed /sc/arion/projects/data-ark/ukb/software/bin/GreedyRelated \
    --rel ${kcl}/ukb18177_rel_s488282.dat \
    --prsice /sc/arion/projects/psychgen/ukb/usr/sam/projects/prs/PRSice/bin/PRSice \
    --glgc /sc/arion/projects/psychgen/ukb/usr/sam/projects/prs/prset/data/sumstats/LDL_GLGC_2013.gz \
    --bgen ${imputed}/ukb18177_imp_chr#_v3.bgen \
    --ifile ${imputed}/ukb_mfi_chr#_v3.txt \
    --sample ${imputed}/ukb18177_imp_chr1_v3_s487283.sample \
    --plink /sc/arion/projects/psychgen2/ukb/sam/prs/diverse_population/software/plink2
