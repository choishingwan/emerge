// This module contains processes for Polygenic score analysis
process transform_summary{
    module 'R'
    label 'small'
    input:
        tuple   val(pop),
                val(cov),
                val(base_maf),
                path(summary)
    output:
        path("LDL-${pop}-${cov}-${base_maf}")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    fread("${summary}") %>% 
        .[,c("PRS.R2", "Null.R2","P")] %>%
        .[, BaseMAF := "${base_maf}"] %>%
        .[, Population := "${pop}" ] %>%
        .[, Covariates := gsub("\\\\+", ",", "${cov}")] %>%
        fwrite(., "LDL-${pop}-${cov}-${base_maf}")
    """
}

process combine_summary{
    publishDir "result", mode: 'copy', overwrite: true
    label 'small'
    module 'R'
    input:
        path("*")
    output:
        path("LDL.result")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    files <- list.files()
    res <- NULL
    for(i in files){
        res %<>% rbind(., fread(i))
    }
    fwrite(res, "LDL.result")
    """
}

process filter_snps{
    label 'normal'
    module 'R'
    input:
        tuple   val(chr),
                path(mfi),
                val(info),
                path(gwas)
    output:
        tuple   val(chr),
                path("chr${chr}.snp")
    script:
    """
     #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    mfi <- fread("${mfi}", header = F) %>%
            .[V8 > ${info}]
    fread("${gwas}") %>%
        .[ rsid %in% mfi[,V2], "rsid"] %>%
        fwrite(., "chr${chr}.snp")
    """
}
process bgen_to_bed{
    label 'huge'
    time '10h'
    input:
        tuple   val(chr),
                path(snp),
                path(bgen),
                path(bgi),
                path(sample),
                path(plink)
    output:
        tuple   path("chr${chr}.bed"),
                path("chr${chr}.bim"),
                path("chr${chr}.fam")
    script:
    """
     ./${plink} \
        --bgen ${bgen} ref-first \
        --sample ${sample} \
        --extract ${snp} \
        --make-bed \
        --out chr${chr} \
        --threads 12
    """

}
process pre_clump{
    label 'big'
    module 'cmake'
    time '10h'
    afterScript 'ls * | grep -v *snp | grep -v *fam | xargs rm'
    input:
        tuple   val(pop),
                path(qc_snp),
                path(qc_fam), 
                path(gwas),
                val(base_maf),
                path(prsice)
        path("*")
    output:
        tuple   val(pop),
                val(base_maf),
                path("LDL-${pop}.snp"),
                path(qc_fam)
    script:
    """
    cat *bim  | cut -f 2 | sort | uniq -u > snp
    awk 'NR==FNR {a[\$1]=\$1}  NR != FNR && \$1 in a {print}' snp ${qc_snp} > qc_snp
    bmaf="--base-maf Freq.A1.1000G.EUR:${base_maf}"
    if [ "${base_maf}" == "0" ];
    then 
        bmaf=""
    fi
    ./${prsice} \
        --target chr# \
        --base ${gwas} \
        --snp rsid \
        --a1 A1 \
        --a2 A2 \
        --beta \
        --stat beta \
        --extract qc_snp \
        --fastscore \
        --bar-levels 1 \
        --no-regress \
        --print-snp \
        --keep ${qc_fam} \
        --pvalue P-value \
        --thread 22 \
        --out LDL-${pop} \
        \${bmaf}
    """
}

process run_prsice{
    label 'more'
    module 'cmake'
    time '10h'
    afterScript 'ls * | grep -v *summary | xargs rm'
    input:
        tuple   val(pop),
                val(covar),
                path(pheno),
                val(base_maf),
                path(qc_snp),
                path(qc_fam), 
                path(gwas),
                path(prsice)
        path("*")
    output:
        tuple   val(pop),
                val(covar),
                val(base_maf),
                path("LDL-${pop}-${covar}.summary")
    script:
    """
    bmaf="--base-maf Freq.A1.1000G.EUR:${base_maf}"
    if [ "${base_maf}" == "0" ];
    then 
        bmaf=""
    fi
    ./${prsice} \
        --target chr# \
        --base ${gwas} \
        --snp rsid \
        --a1 A1 \
        --a2 A2 \
        --beta \
        --stat beta \
        --pheno ${pheno} \
        --extract ${qc_snp} \
        --no-clump \
        --keep ${qc_fam} \
        --pvalue P-value \
        --out LDL-${pop}-${covar} \
        --ultra \
        \${bmaf}
    """
}
