#!/usr/bin/env nextflow
nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.1'

// Do residualization inverse normalize in all samples together
timestamp='2020-11-04'
if(params.version) {
    System.out.println("")
    System.out.println("Cross-population LDL analysis using UK Biobank - Version: $version ($timestamp)")
    exit 1
}
// default values
params.geno = 0.02
params.seed = 1234
params.kmean = 4
params.hwe = 1e-8
params.maf = 0.01
params.build = "grch37"
params.windSize = 200
params.windStep = 50
params.r2 = 0.2
params.maxSize = 10000
params.sex = "sd"
params.sexSD = 3
params.maleF = 0.8
params.femaleF = 0.2
params.thres = 0.044

params.extreme=1
params.normal=5
params.perm=10000
if(params.help){
    System.out.println("")
    System.out.println("Cross-population LDL analysis using UK Biobank - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run ldl_analysis.nf [options]")
	System.out.println("Mandatory arguments:")
    System.out.println("    --bgen        BGEN file prefix. Use # to represent chromosome number")
    System.out.println("    --db          UKB Phenotype database")
    System.out.println("    --drop        Samples who withdrawn consent")
    System.out.println("    --geno        Genotype file prefix ")
    System.out.println("    --greed       GreedyRelated executable")
    System.out.println("    --glgc        GWAS summary statistic from GLGC")
    System.out.println("    --ifile       File contain the info score information")
    System.out.println("    --prsice      PRSice executable")
    System.out.println("    --plink       PLINK 2.0 executable")
    System.out.println("    --rel         Path to the relatedness data file")
    System.out.println("    --sample      Sample file for bgen")
    System.out.println("    --sqc         Path to the UK biobank SQC file")
    System.out.println("Filtering parameters:")
    System.out.println("    --geno        Genotype missingness. Default: ${params.geno}")
    System.out.println("    --kmean       Number of kmean for pca clustering. Default: ${params.kmean}")
    System.out.println("    --maf         MAF filtering for sample filtering. Default: 0.01")
    System.out.println("    --hwe         HWE filtering. Default: ${params.hwe}")
    System.out.println("    --build       Genome build. Can either be grch37 or grch38. ")
    System.out.println("                  Use to define long LD regions. Default: ${params.build}")
    System.out.println("    --windSize    Window size for prunning. Default: ${params.windSize}")
	System.out.println("    --windStep    Step size for prunning. Default: ${params.windStep}")
    System.out.println("    --r2          Threshold for prunning. Default: ${params.r2}")
    System.out.println("    --maxSize     Maxnumber of samples used for prunning. Default: ${params.maxSize}")
    System.out.println("    --sex         sd or fix.")
    System.out.println("                  sd: exclude samples N sd away from mean, as defined by --sexSD")
    System.out.println("                  fix: exclude male > --maleF and female < --femaleF")
    System.out.println("                  Default: ${params.sex}")
    System.out.println("    --sexSD       Sample with Fstat X SD higher (female)/ lower")
    System.out.println("                  (male) from the mean are filtered. Default: ${params.sexSD}")
    System.out.println("    --maleF       F stat threshold for male. Male with F stat lower")
    System.out.println("                  than this number will be removed. Default: ${params.maleF}")
    System.out.println("    --femaleF     F stat threshold for female. Female with F stat higher")
    System.out.println("                  than this number will be removed. Default: ${params.femaleF}")
    System.out.println("    --relThres    Threshold for removing related samples. Default: ${params.thres}")
    System.out.println("Options:")
    System.out.println("    --seed        Seed for random algorithms. Default: ${params.seed}")
    System.out.println("    --help        Display this help messages")
    exit 1
} 

// module inclusion
include {   extract_sqc;
            get_country_of_birth;
            get_populations;
            extract_ldl_from_sql;
            ldl_adjustment;
            residualize_phenotype } from './phenotype'
include {   first_pass_geno;
            basic_qc;
            generate_high_ld_region;
            prunning;
            calculate_sex_fstat;
            filter_sex_mismatch;
            relatedness_filtering;
            finalize_data; } from './quality_control'

include {   pre_clump;
            run_prsice; 
            transform_summary;
            combine_summary;
            bgen_to_bed;
            filter_snps; } from './prs_analysis'

// Helper function
def fileExists = { fn ->
   if (fn.exists())
       return fn;
    else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}

def gen_file(a, bgen){
    return bgen.replaceAll("#",a.toString())
}
def gen_idx(a, bgen){
    return gen_file(a, bgen)+".bgi"
}

def get_chr(a, bgen){
    if(bgen.contains("#")){
        return a
    }
    else {
        return 0
    }
}
// define variables

bgen = Channel.value(1..22)
    .flatten()
    .map{ a -> [ get_chr(a, "${params.bgen}"), file(gen_file(a, "${params.bgen}")), file(gen_idx(a, "${params.bgen}"))]}
    .unique()
genotype = Channel
    .fromFilePairs("${params.bfile}.{bed,bim,fam}",size:3, flat : true){ file -> file.baseName }  
    .ifEmpty { error "No matching plink files" }        
    .map { a -> [fileExists(a[1]), fileExists(a[2]), fileExists(a[3])] } 
greed = Channel.fromPath("${params.greed}")
gwas = Channel.fromPath("${params.glgc}")
mfi = Channel.value(1..22)
    .flatten()
    .map{ a -> [get_chr(a, "${params.ifile}"), file(gen_file(a, "${params.ifile}"))]}
    .unique()
plink = Channel.fromPath("${params.plink}")
prsice = Channel.fromPath("${params.prsice}")
rel = Channel.fromPath("${params.rel}")
sample = Channel.fromPath("${params.sample}")
sqc = Channel.fromPath("${params.sqc}")
sql=Channel.fromPath("${params.db}")
withdrawn=Channel.fromPath("${params.dropout}")

workflow{
    // 1. First, we need to extract the covariate information from UKB
    extract_covariate()
    // 2. Group samples into the 4 population (East Asia will likely not have enough samples)
    extract_population(extract_covariate.out.cov)
    // 3. Now extract the phenotype for each population (we might lost Asia)
    extract_ldl(    extract_population.out,
                    extract_covariate.out.cov)
    // 4. For each population, perform the PLINK QC
    quality_control_pipline(extract_ldl.out,
                            extract_covariate.out.het, 
                            extract_covariate.out.sex)
    // 5. Convert BGEN file to plink format to speed up downstream analysis
    convert_files()
    // 6. Perform polygenic scoring
    polygenic_score_pipeline(   extract_ldl.out,
                                quality_control_pipline.out,
                                convert_files.out)
}

workflow extract_covariate{
    main:
        extract_sqc(sqc, "${params.out}")
    emit:
        het = extract_sqc.out.het
        cov = extract_sqc.out.covar 
        sex = extract_sqc.out.sex
}


workflow extract_population{
    take: covar
    main:
        // 1. First, get country of birth information 
        get_country_of_birth(sql)
        // 2. Extract population based on 4 mean clustering. 
        //    Cluster contains most samples from X are defined
        //    as cluster of X
        get_populations(
            get_country_of_birth.out, 
            covar, 
            params.kmean, 
            params.seed,
            params.out)
        // 3. Group populations together into a single channel so 
        //    that it is easier to handle for downstream analyses
        populations = get_populations.out.eur \
            | mix(get_populations.out.afr) \
            | mix(get_populations.out.asia)
    emit:
        populations
}

workflow extract_ldl{
    take: population
    take: covar
    main:
        // 1. First, extract LDL and corresponding covariates from the SQL
        extract_ldl_from_sql(sql)
        // 2. Merge the covariates and LDL phenotype together, 
        //    remove samples with missing phenotype
        population \
            | combine(extract_ldl_from_sql.out) \
            | combine(covar) \
            | combine(withdrawn) \
            | ldl_adjustment
    emit:
        ldl_adjustment.out
}

workflow quality_control_pipline{
    take: phenotype
    take: het
    take: sex
    main:
        // 1. Do the first pass genotype missingness filtering
        phenotype \
            | combine(genotype) \
            | combine(het) \
            | combine(Channel.of(params.geno)) \
            | first_pass_geno 
        // 2. Do second pass filtering
        phenotype \
            | combine(first_pass_geno.out.snp, by: 0) \
            | combine(het) \
            | combine(genotype) \
            | combine(Channel.of("${params.hwe}")) \
            | combine(Channel.of("${params.geno}")) \
            | combine(Channel.of("${params.maf}")) \
            | basic_qc
        // 3. Extract high LD region for prunning 
        basic_qc.out \
            | combine(genotype) \
            | combine(Channel.of("${params.build}")) \
            | generate_high_ld_region
        // 4. Perform prunning
            basic_qc.out \
                | combine(generate_high_ld_region.out, by: 0)\
                | combine(genotype) \
                | combine(Channel.of("${params.windSize}")) \
                | combine(Channel.of("${params.windStep}")) \
                | combine(Channel.of("${params.r2}")) \
                | combine(Channel.of("${params.maxSize}")) \
                | combine(Channel.of("${params.seed}")) \
                | prunning
        // 5. Calculate F-statistic for Sex check 
        basic_qc.out \
            | combine(prunning.out, by: 0)\
            | combine(genotype) \
            | calculate_sex_fstat
        // 6. Filter out sample with mismatch sex information
        basic_qc.out \
            | combine(calculate_sex_fstat.out, by: 0) \
            | combine(sex) \
            | combine(Channel.of("${params.sex}")) \
            | combine(Channel.of("${params.sexSD}")) \
            | combine(Channel.of("${params.maleF}")) \
            | combine(Channel.of("${params.femaleF}")) \
            | filter_sex_mismatch
        // 7. Extract samples with relative in data using GreedyRelated
        filter_sex_mismatch.out.valid \
            | combine(greed) \
            | combine(rel) \
            | combine(Channel.of("${params.thres}")) \
            | combine(Channel.of("${params.seed}")) \
            | relatedness_filtering
        // 8. Obtain the final set of samples
          basic_qc.out.qc \
            | combine(filter_sex_mismatch.out.mismatch, by: 0)\
            | combine(relatedness_filtering.out, by:0) \
            | combine(genotype) \
            | combine(Channel.of("${params.out}")) \
            | finalize_data
    emit: 
        finalize_data.out
}

workflow convert_files{
    main:
        mfi \
            | combine(Channel.of(0.8)) \
            | combine(gwas) \
            | filter_snps \
            | combine(bgen, by: 0) \
            | combine(sample) \
            | combine(plink) \
            | bgen_to_bed
    emit:
        bgen_to_bed.out
}

workflow polygenic_score_pipeline{
    take: phenotype
    take: qc
    take: imputed
    main:
        // 0 = no filtering
        baseMAF=Channel.of("0.01")
        // will always do Batch Centre and 40 PCs
        covFormula = Channel.of("Sex",
                                "Age",
                                "Fasting",
                                "Dilution",
                                "BMI",
                                "Sex+Age",
                                "Sex+Fasting",
                                "Sex+Dilution",
                                "Sex+BMI",
                                "Sex+Age+Fasting",
                                "Sex+Age+Dilution",
                                "Sex+Age+BMI",
                                "Sex+Age+Fasting+Dilution",
                                "Sex+Age+Fasting+BMI",
                                "Sex+Age+Dilution+BMI",
                                "Sex+Age+Dilution+BMI+Fasting",
                                "Sex+Dilution+BMI",
                                "Sex+Dilution+Fasting",
                                "Sex+Dilution+BMI+Fasting",
                                "Sex+BMI+Fasting",
                                "Age+Fasting",
                                "Age+Dilution",
                                "Age+BMI",
                                "Age+Fasting+Dilution",
                                "Age+Fasting+BMI",
                                "Age+Dilution+BMI",
                                "Age+Fasting+BMI+Dilution",
                                "Fasting+Dilution",
                                "Fasting+BMI",
                                "Fasting+Dilution+BMI",
                                "Dilution+BMI")
        clump_input = qc \
            | combine(gwas) \
            | combine(baseMAF) \
            | combine(prsice) 
        pre_clump(clump_input, imputed.flatten().collect())
        prs_input = phenotype \
            | combine(covFormula) \
            | residualize_phenotype \
            | combine(pre_clump.out, by: 0) \
            | combine(gwas) \
            | combine(prsice) 
        run_prsice(prs_input, imputed.flatten().collect()) \
            | transform_summary \
            | collect \
            | combine_summary
}
