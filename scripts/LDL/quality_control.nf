// This module contains all processess relating to quality control

process first_pass_geno{
    cpus 12
    module 'plink/1.90b6.7'
    executor 'lsf'
    memory '1G'
    input:
        tuple   val(pop),
                path(pheno),
                path(bed),
                path(bim),
                path(fam),
                path(invalid),
                val(geno)
    output:
        tuple   val(pop),
                path("${pop}-geno${geno}.snplist"), emit: snp
    script:

    """
    plink   --bed ${bed} \
            --bim ${bim} \
            --fam ${fam} \
            --geno ${geno} \
            --remove ${invalid} \
            --write-snplist \
            --keep ${pheno} \
            --out ${pop}-geno${geno}
    """
}


process basic_qc{
    cpus 12
    module 'plink/1.90b6.7'
    executor 'lsf'
    memory '1G'
    input:
        tuple   val(pop),
                path(pheno),
                path(snplist), 
                path(invalid),
                path(bed),
                path(bim),
                path(fam),
                val(hwe),
                val(geno),
                val(maf)
    output:
        tuple   val(pop), 
                path("${pop}-basic-qc.fam"),  
                path("${pop}-basic-qc.snplist"), emit: qc
    script:
    base=bed.baseName
    """
    plink   --keep ${pheno} \
            --bed ${bed} \
            --bim ${bim} \
            --fam ${fam} \
            --geno ${geno} \
            --hwe ${hwe} \
            --maf ${maf} \
            --write-snplist \
            --make-just-fam \
            --out ${pop}-basic-qc \
            --extract ${snplist} \
            --remove ${invalid} 
    """
}


process generate_high_ld_region{
    cpus 12
    module 'plink/1.90b6.7'
    memory '1G'
    executor 'lsf'
    input:
        tuple   val(pop),
                path(sample),
                path(qc_snp),
                path(bed),
                path(bim),
                path(fam),
                val(build)
    output:
        tuple   val(pop),
                path("${pop}.set")
    script:
    base=bed.baseName
    """
    echo "1     48000000     52000000   High_LD
2     86000000     100500000    High_LD
2     134500000     138000000   High_LD
2     183000000     190000000   High_LD
3     47500000     50000000 High_LD
3     83500000     87000000 High_LD
3     89000000     97500000 High_LD
5     44500000     50500000 High_LD
5     98000000     100500000    High_LD
5     129000000     132000000   High_LD
5     135500000     138500000   High_LD
6     25000000     35000000 High_LD
6     57000000     64000000 High_LD
6     140000000     142500000   High_LD
7     55000000     66000000 High_LD
8     7000000     13000000  High_LD
8     43000000     50000000 High_LD
8     112000000     115000000   High_LD
10     37000000     43000000    High_LD
11     46000000     57000000    High_LD
11     87500000     90500000    High_LD
12     33000000     40000000    High_LD
12     109500000     112000000  High_LD
20     32000000     34500000 High_LD" > high_ld_37
    echo "1     48060567     52060567     hild
2     85941853     100407914     hild
2     134382738     137882738     hild
2     182882739     189882739     hild
3     47500000     50000000     hild
3     83500000     87000000     hild
3     89000000     97500000     hild
5     44500000     50500000     hild
5     98000000     100500000     hild
5     129000000     132000000     hild
5     135500000     138500000     hild
6     25500000     33500000     hild
6     57000000     64000000     hild
6     140000000     142500000     hild
7     55193285     66193285     hild
8     8000000     12000000     hild
8     43000000     50000000     hild
8     112000000     115000000     hild
10     37000000     43000000     hild
11     46000000     57000000     hild
11     87500000     90500000     hild
12     33000000     40000000     hild
12     109521663     112021663     hild
20     32000000     34500000     hild
X     14150264     16650264     hild
X     25650264     28650264     hild
X     33150264     35650264     hild
X     55133704     60500000     hild
X     65133704     67633704     hild
X     71633704     77580511     hild
X     80080511     86080511     hild
X     100580511     103080511     hild
X     125602146     128102146     hild
X     129102146     131602146     hild" > high_ld_38
    ldFile=high_ld_37
    if [[ "${build}" != "grch37" ]];
    then
        ldFile=high_ld_38
    fi
    echo \${ldFile}
    plink \
        --bed ${bed} \
        --bim ${bim} \
        --fam ${fam} \
        --extract ${qc_snp} \
        --keep ${sample} \
        --make-set \${ldFile} \
        --write-set \
        --out ${pop}
    """
}


process prunning{
    cpus 12
    memory '1G'
    executor 'lsf'
    module 'plink/1.90b6.7'
    input: 
        tuple   val(pop),
                path(sample),
                path(qc_snp),
                path(high_ld),
                path(bed),
                path(bim),
                path(fam),
                val(wind_size),
                val(wind_step),
                val(wind_r2),
                val(max_size),
                val(seed)
    output:
        tuple   val(pop),
                path ("${pop}-qc.prune.in")
    script:
    base=bed.baseName
    """
    if [[ \$(wc -l < ${sample}) -ge ${max_size} ]];
    then
        plink \
            --bed ${bed} \
            --bim ${bim} \
            --fam ${fam} \
            --extract ${qc_snp} \
            --keep ${sample} \
            --indep-pairwise ${wind_size} ${wind_step} ${wind_r2} \
            --out ${pop}-qc \
            --thin-indiv-count ${max_size} \
            --seed ${seed} \
            --exclude ${high_ld}
    else
        plink \
            --bed ${bed} \
            --bim ${bim} \
            --fam ${fam} \
            --extract ${qc_snp} \
            --keep ${sample} \
            --indep-pairwise ${wind_size} ${wind_step} ${wind_r2} \
            --out ${pop}-qc \
            --exclude ${high_ld}
    fi 
    """
}  

process calculate_sex_fstat{
    cpus 12
    memory '1G'
    module 'plink/1.90b6.7'
    executor 'lsf'
    input:
        tuple   val(pop),
                path(qc_fam),
                path(qc_snp),
                path(prune),
                path(bed),
                path(bim),
                path(fam)
    output:
        tuple   val(pop),
                path ("${pop}.sexcheck")
    script:
    base=bed.baseName
    """
    plink \
        --bed ${bed} \
        --bim ${bim} \
        --fam ${fam} \
        --extract ${prune} \
        --keep ${qc_fam} \
        --check-sex \
        --out ${pop}
    """
}

process filter_sex_mismatch{
    cpus 1
    executor 'lsf'
    memory '1G'
    module 'R'
    input:
        tuple   val(pop),
                path(qc_fam),
                path(qc_snp),
                path(fstat),
                path(biosex),
                val(mode),
                val(sdm),
                val(male),
                val(female)
    output:
        tuple   val(pop),
                path ("${pop}.sex-mismatch"), emit: mismatch
        tuple   val(pop),
                path( "${pop}.sex-valid"), emit: valid
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    fam <- fread("${qc_fam}")
    # Read in sex information and remove samples that doesn't pass QC
    sex <- fread("${biosex}") %>%
        merge(., fread("${fstat}")) %>%
        .[FID %in% fam[,V1] & FID > 0]
    filter.info <- NULL
    if("${mode}"=="sd"){
        filter.info <-  "(${sdm} ${mode} from mean)"
        sex.bound <- sex[,.(m=mean(F), s=sd(F)), by="Submitted.Gender"]
        sex[,invalid := FALSE]
        bound <- sex.bound[Submitted.Gender=="M"]
        sex[   Submitted.Gender=="M" &
            ( F < bound[,m] -bound[,s]*${sdm} ), invalid:=TRUE]
        bound <- sex.bound[Submitted.Gender=="F"]
        sex[   Submitted.Gender=="F" &
            (F > bound[,m] + bound[,s]*${sdm}), invalid:=TRUE]
    }else{
        filter.info <- "(Male fstat >${male}; Female fstat < ${female} )"
        sex[,invalid:=FALSE]
        sex[Submitted.Gender=="M" & F < ${male}, invalid:=TRUE ]
        sex[Submitted.Gender=="F" & F < ${female}, invalid:=TRUE ]
    }
    
    invalid <- sex[invalid==TRUE]
    fwrite(invalid, "${pop}.sex-mismatch", sep="\\t")
    fwrite(sex[invalid==FALSE], "${pop}.sex-valid", sep="\\t")
    """
}


process relatedness_filtering{
    cpus 1
    executor 'lsf'
    memory '10G'
    time '1h'
    module 'cmake'
    input: 
        tuple   val(pop),
                path(samples),
                path(greedy),
                path(related),
                val(thres),
                val(seed)
    output:
        tuple   val(pop),
                path ("${pop}-invalid.samples")

    script:
    """
    ./${greedy} \
        -r ${related} \
        -i ID1 \
        -I ID2 \
        -f Kinship \
        -k ${samples} \
        -o ${pop}-invalid.samples \
        -t ${thres} \
        -s ${seed}
    
    """
}

process finalize_data{
    publishDir "genotype", mode: 'copy', overwrite: true
    cpus 12
    module 'plink/1.90b6.7'
    memory '1G'
    executor 'lsf'
    input:
        tuple   val(pop),
                path(qc_fam),
                path(qc_snp),
                path(sex),
                path(rel),
                path(bed),
                path(bim),
                path(fam),
                val(out)
    output:
        tuple   val(pop),
                path("${out}-${pop}-qc.snplist"),
                path("${out}-${pop}-qc.fam"), emit: qced
    script:
    base=bed.baseName
    """
    cat ${rel} ${sex} > ${out}.removed
    plink \
        --bed ${bed} \
        --bim ${bim} \
        --fam ${fam} \
        --extract ${qc_snp} \
        --keep ${qc_fam} \
        --remove ${out}.removed \
        --make-just-fam \
        --write-snplist \
        --out ${out}-${pop}-qc
    """


}


