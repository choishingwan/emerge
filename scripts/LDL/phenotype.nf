// This module contains processes responsible for phenotype extraction

process extract_sqc{
    publishDir "phenotype", mode: 'copy', overwrite: true
    module 'R'
    input:
        path(sqc)
        val(out)
    output:
        path "${out}.invalid", emit: het
        path "${out}.sex", emit: sex
        path "${out}.covar", emit: covar
        path "${out}-het.meta", emit: meta
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    dat <- fread("${sqc}")
    dat[,c("FID", "IID", "Batch", paste("PC",1:40,sep=""))] %>%
        fwrite(., "${out}.covar", quote=F, na="NA", sep="\\t")
    dat[,c("FID", "IID", "Submitted.Gender")] %>%
        fwrite(., "${out}.sex", quote=F, na="NA", sep="\\t")
    het <- dat[het.missing.outliers==1 | excess.relatives==1 | FID < 0] %>%
        .[,c("FID", "IID")] 
        fwrite(het , "${out}.invalid", quote=F, na="NA", sep="\\t")
    fileConn <- file("${out}-het.meta")
    writeLines(paste0("2. ",nrow(het),"sample(s) with excessive het, missing or relatives"), fileConn)
    close(fileConn)
    """
}

process get_country_of_birth{
    publishDir "population", mode: 'copy', overwrite: true
    input:
        path(db)
    output:
        path("population.csv")
    script:
    """
    echo "
    .mode csv
    .header on
    .output population.csv
    SELECT  s.sample_id AS FID,
            s.sample_id AS IID,
            trait.pheno AS Country
    FROM    Participant s
            LEFT JOIN   f20115 trait ON
                        s.sample_id=trait.sample_id
                        AND trait.instance = 0;
    .quit
        " > sql;
    sqlite3 ${db} < sql
    """
}


process get_populations{
    publishDir "population", mode: 'copy', overwrite: true
    module 'R'
    input:
        path(pop)
        path(covar)
        val(kmean)
        val(seed)
        val(out)
    output:
        tuple   val("European"),
                path("${out}-${kmean}mean-EUR"), emit: eur
        tuple   val("African"),
                path("${out}-${kmean}mean-AFR"), emit: afr
        tuple   val("Asian"),
                path("${out}-${kmean}mean-ASIA"), emit: asia
        tuple   path("${out}-pca.png"),
                path("${out}-pca-select.png"), emit: plot
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(forcats)
    library(magrittr)
    library(ggplot2)
    library(ggsci)
    # Read in covariate
    cov <- fread("${covar}")
    # Read in population information. Group samples into continent 
    pop <- fread("${pop}") %>%
        .[, Country := floor(as.numeric(Country)/100)] %>%
        .[, Population := as.factor(Country)] %>%
        .[, Population := fct_recode(Population, 
                        "Africa" = "1",
                        "Asia" = "2",
                        "Europe" = "3",
                        "NAmerica" = "4",
                        "Oceania" = "5",
                        "SAmerica" = "6")] %>%
        .[is.na(Population), Population := "UK"] %>%
        .[, -c("Country")]

    # Set the seed for the kmean clustering
    if("${seed}"!="false"){
        set.seed("${seed}")
    }
    # Perform kmean clustering
    pc1k<-kmeans(cov[,PC1], ${kmean})
    pc2k<-kmeans(cov[,PC2], ${kmean})
    cov <- cov[,clusters:=as.factor(paste(pc1k\$cluster,pc2k\$cluster,sep="."))] %>%
        .[,c("FID", "IID", "clusters", "PC1","PC2")]
    population <- merge(cov, pop)
    # Plot PCA plot
    g <- ggplot(cov, aes(x=PC1,y=PC2, color=clusters))+\
            geom_point()+\
            theme_classic()+\
            scale_color_npg()+\
            theme(  legend.text = element_text(size=14),
                    legend.title = element_text(size=16, face="bold"),
                    axis.title = element_text(size=16, face="bold"),
                    axis.text = element_text(size=14))
    ggsave("${out}-pca.png", plot=g, height=7,width=7)

    # We assume the largest cluster is the EUR cluster
    max.cluster <- names(which.max(table(cov[,clusters])))
    eur <- cov[clusters==max.cluster,c("FID", "IID")]
    fwrite(eur, "${out}-${kmean}mean-EUR", quote=F, na="NA", sep="\\t")

    # Then we assume the cluster with most African be the african cluster
    afr.cluster <- names(which.max(table(population[Population=="Africa", clusters])))
    afr <- cov[clusters==afr.cluster,c("FID", "IID")]
    fwrite(afr, "${out}-${kmean}mean-AFR", quote=F, na="NA", sep="\\t")

    # Similarly, we assume the cluster with most Asian are the Asian cluster
    asia.cluster <- names(which.max(table(population[Population=="Asia", clusters])))
    asia <- cov[clusters==asia.cluster,c("FID", "IID")]
    fwrite(asia, "${out}-${kmean}mean-ASIA", quote=F, na="NA", sep="\\t")

    cov[,Group:="Not selected"]
    cov[clusters==max.cluster, Group:="European"]
    cov[clusters==afr.cluster, Group:="African"]
    cov[clusters==asia.cluster, Group:="Asian"]
    g <- ggplot(cov, aes(x=PC1,y=PC2, color=Group))+\
            geom_point()+\
            theme_classic()+\
            scale_color_npg()+\
            theme(  legend.text = element_text(size=14),
                    legend.title = element_text(size=16, face="bold"),
                    axis.title = element_text(size=16, face="bold"),
                    axis.text = element_text(size=14))
    ggsave("${out}-pca-select.png", plot=g, height=7,width=7)
    """
}

process extract_ldl_from_sql{
    label 'normal'
    input:
        path(db)
    output:
        path("ldl.csv")
    script:
    """
    echo "
    .mode csv
    .header on 
    .output ldl.csv
    SELECT  s.sample_id AS FID,
            s.sample_id AS IID, 
            age.pheno AS Age,
            sex.pheno AS Sex,
            centre.pheno AS Centre,
            fasting.pheno AS Fasting,
            dilution.pheno AS Dilution,
            trait.pheno AS Phenotype,
            bmi.pheno AS BMI,
            MAX(
                CASE WHEN med.instance = 0 THEN 
                    CASE
                    WHEN
                        med.pheno in (1141146234, 1141192414, 1140910632,
                                        1140888594, 1140864592, 1141146138,
                                        1140861970, 1140888648, 1141192410,
                                        1141188146, 1140861958, 1140881748,
                                        1141200040, 1140861922)
                    THEN 1
                    ELSE 0
                    END
                END
            ) AS Statin
    FROM    Participant s 
            JOIN        f20003 med ON 
                        s.sample_id=med.sample_id 
            LEFT JOIN   f30780 trait ON
                        s.sample_id=trait.sample_id
                        AND trait.instance = 0
            LEFT JOIN   f31 sex ON
                        s.sample_id=sex.sample_id 
                        AND sex.instance = 0
            LEFT JOIN   f21003 age ON 
                        s.sample_id=age.sample_id 
                        AND age.instance = 0
            LEFT JOIN   f54 centre ON 
                        s.sample_id=centre.sample_id 
                        AND centre.instance = 0
            LEFT JOIN   f74 fasting ON
                        s.sample_id=fasting.sample_id
                        AND fasting.instance = 0
            LEFT JOIN   f30897 dilution ON
                        s.sample_id=dilution.sample_id
                        AND dilution.instance = 0
            LEFT JOIN   f21001 bmi ON
                        s.sample_id=bmi.sample_id 
                        AND bmi.instance = 0
    GROUP BY s.sample_id;
    .quit
    " > sql;
    sqlite3 ${db} < sql
    """
}


process ldl_adjustment{
    module 'R'
    label 'normal'
    input:
        tuple   val(pop),
                path(sample),
                path(pheno),
                path(cov),
                path(withdrawn)

    output:
    tuple   val(pop),
            path("LDL-${pop}.pheno") optional true
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    withdrawn <- fread("${withdrawn}", header = F)
    fam <- fread("${sample}") %>%
        .[!IID %in% withdrawn[, V1]]
    cov <- fread("${cov}") %>%
        .[FID %in% fam[,IID]]
    pheno <- fread("${pheno}") %>%
        .[Statin == 0] %>%
        merge(., cov) %>%
        na.omit %>%
        .[, Centre := as.factor(Centre)] %>%
        .[, Batch := as.factor(Batch)]
    if (nrow(pheno) < 1000 | length(unique(pheno[, Phenotype])) == 1) {
        
    } else{
        pheno %>%
            # Remove outliers
            .[Phenotype > mean(Phenotype) - 6 * sd(Phenotype) &
                Phenotype < mean(Phenotype) + 6 * sd(Phenotype)]  %>%
            na.omit %>%
            fwrite(., "LDL-${pop}.pheno", sep = "\\t")
    }
    """
}

process residualize_phenotype{
    module 'R'
    label "normal"
    input:
        tuple   val(pop),
                path(pheno),
                val(covariate)
    output:
        tuple   val(pop),
                val(covariate),
                path("LDL-${pop}.resid")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    formula <- paste("PC",1:40,collapse="+",sep="") %>%
        paste("Phenotype~Batch+Centre+${covariate}+",.) %>%
        as.formula
    pheno <- fread("${pheno}") %>%
        na.omit %>%
        .[, Centre := as.factor(Centre)] %>%
        .[, Batch := as.factor(Batch)] %>%
        .[, Pheno := lm(formula, data=.) %>%
                        rstandard] %>%
            .[, c("FID", "IID", "Pheno")] %>%
            na.omit %>%
            fwrite(., "LDL-${pop}.resid", sep = "\\t")
    """
}
